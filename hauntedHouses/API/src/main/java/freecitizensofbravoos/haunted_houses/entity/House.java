package freecitizensofbravoos.haunted_houses.entity;

import freecitizensofbravoos.haunted_houses.Address;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author xkostoln
 */
@Entity
public class House implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable=true)
    private String name;

    @Embedded
    private Address address;

    @Temporal(TemporalType.DATE)
    private Date startOfHaunting;

    @Column(nullable=true)
    private String history;

    @OneToMany(mappedBy="house", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<Human> humans = new ArrayList<Human>();

    @OneToMany(mappedBy="house", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<Ghost> ghosts = new ArrayList<Ghost>();    
     
    public void addHuman(Human human) {
        if(human == null)
        {
            throw new IllegalArgumentException("Human cannot be null!");
        }        
        humans.add(human);
        human.setHouse(this);
    }

    public void removeHuman(Human human) {
        humans.remove(human); 
        human.setHouse(null);
    }

    public void addGhost(Ghost ghost) {
        if(ghost == null)
        {
            throw new IllegalArgumentException("Ghost cannot be null!");
        }
        ghosts.add(ghost);
        ghost.setHouse(this);
    }

    public void removeGhost(Ghost ghost) {
        ghosts.remove(ghost);
        ghost.setHouse(null);
    }
   
    public Long getId() {
        return id;
    }

    @Override
    public String toString() {
        return "House{" + "id=" + id + ", name=" + name + ", address=" + address + ", startOfScaring=" + startOfHaunting + ", history=" + history + ", humans=" + humans + '}';
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public List<Human> getHumans() {
        return new ArrayList<>(humans);
    }

    public void setHumans(List<Human> humans) {
        this.humans = humans;
    }

    public List<Ghost> getGhosts() {
        return new ArrayList<>(ghosts);
    }

    public void setGhosts(List<Ghost> ghosts) {
        this.ghosts = ghosts;
    }    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final House other = (House) obj;
        return Objects.equals(this.id, other.id);
    }   

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getStartOfHaunting() {
        return startOfHaunting;
    }

    public void setStartOfHaunting(Date startOfHaunting) {
        this.startOfHaunting = startOfHaunting;
    }  

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    } 
}
