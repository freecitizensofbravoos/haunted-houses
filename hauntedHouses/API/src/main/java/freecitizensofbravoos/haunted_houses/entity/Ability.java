package freecitizensofbravoos.haunted_houses.entity;

import freecitizensofbravoos.haunted_houses.Severity;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 *
 * @author xmoravc6
 */
@Entity
public class Ability implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
    
    @Column(nullable=false)
    private String name;
    
    @Column(nullable=true)
    private String description;
    
    @Enumerated
    private Severity severity;    
    
    @ManyToMany(fetch = FetchType.LAZY, mappedBy="abilities")
    private Set<Ghost> ghosts = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public Set<Ghost> getGhosts() {
        return new HashSet<>(ghosts);
    }

    protected void setGhosts(Set<Ghost> ghosts) {
        this.ghosts = ghosts;
    }
    
    public boolean addGhost(Ghost g)
    {
        if(g == null)
        {
            throw new IllegalArgumentException("Ghost cannot be null!");
        }        
        return ghosts.add(g);
    }
    
    public boolean removeGhost(Ghost g)
    {
        return ghosts.remove(g);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 13 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 13 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 13 * hash + (this.severity != null ? this.severity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ability other = (Ability) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        return this.severity == other.severity;
    }

    @Override
    public String toString() {
        return "Ability{" + "id=" + id + ", name=" + name + ", description=" + description + ", severity=" + severity + '}';
    }
}
