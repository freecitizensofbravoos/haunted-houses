package freecitizensofbravoos.haunted_houses.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author xkaiser1
 */

@Entity
public class Ghost implements Serializable {    
    @Id
    @GeneratedValue
    @Column(name="ID")
    private Long id;
    
    @Column(nullable=true)
    private String name;
    
    @Temporal(TemporalType.TIME)
    private Date beginningOfHaunt;
    
    @Temporal(TemporalType.TIME)
    private Date endOfHaunt;
    
    @Column(nullable=true)
    private int energyToHaunt;
    
    @Column(nullable=true)
    private int numberOfPeopleHaunted;
   
    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable (
        name="ghosts_abilities",
        joinColumns={@JoinColumn(name="ghost_ID", referencedColumnName="ID")},
        inverseJoinColumns={@JoinColumn(name="ability_ID", referencedColumnName="ID")}
    )
    private Set<Ability> abilities = new HashSet<Ability>();
    
    @ManyToOne
    private House house;    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginningOfHaunt() {
        return beginningOfHaunt;
    }

    public void setBeginningOfHaunt(Date beginningOfHaunt) {
        this.beginningOfHaunt = beginningOfHaunt;
    }

    public Date getEndOfHaunt() {
        return endOfHaunt;
    }

    public void setEndOfHaunt(Date endOfHaunt) {
        this.endOfHaunt = endOfHaunt;
    }

    public int getEnergyToHaunt() {
        return energyToHaunt;
    }

    public void setEnergyToHaunt(int energyToHaunt) {
        this.energyToHaunt = energyToHaunt;
    }

    public int getNumberOfPeopleHaunted() {
        return numberOfPeopleHaunted;
    }

    public void setNumberOfPeopleHaunted(int numberOfPeopleHaunted) {
        this.numberOfPeopleHaunted = numberOfPeopleHaunted;
    }

    public Set<Ability> getAbilities() {
        return new HashSet<>(abilities);
    }

    protected void setAbilities(Set<Ability> abilities) {
        this.abilities = abilities;
    }
    
    public boolean addAbility(Ability a)
    {
        if(a == null)
        {
            throw new IllegalArgumentException("Ability cannot be null!");
        }        
        return abilities.add(a);
    }
    
    public boolean removeAbility(Ability a)
    {
        return abilities.remove(a);
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ghost other = (Ghost) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Ghost{" + "id=" + id + ", name=" + name + ", beginningOfHaunt=" + beginningOfHaunt + ", endOfHaunt=" + endOfHaunt + ", energyToHaunt=" + energyToHaunt + ", numberOfPeopleHaunted=" + numberOfPeopleHaunted + ", abilities=" + abilities + '}';
    }
}
