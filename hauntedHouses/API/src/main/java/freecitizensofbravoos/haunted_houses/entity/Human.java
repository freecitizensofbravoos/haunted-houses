package freecitizensofbravoos.haunted_houses.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author xurge
 */
@Entity
public class Human implements Serializable {
    @Id    
    @GeneratedValue
    private Long id;

    @Column(nullable=true)
    private String name;
    
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;
        
    @Column(nullable=true)
    private String afraidOf;
        
    @Column(nullable=true)
    private boolean medium;
    
    @ManyToOne
    private House house;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAfraidOf() {
        return afraidOf;
    }

    public void setAfraidOf(String afraidOf) {
        this.afraidOf = afraidOf;
    }

    public boolean isMedium() {
        return medium;
    }

    public void setMedium(boolean medium) {
        this.medium = medium;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Human other = (Human) obj;
        return !(!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id)));
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    } 

    @Override
    public String toString() {
        return "Human{" + "id=" + id + ", name=" + name + ", dateOfBirth=" + dateOfBirth + ", afraidOf=" + afraidOf + ", medium=" + medium + '}';
    }
}
