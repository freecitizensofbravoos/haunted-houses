package freecitizensofbravoos.haunted_houses;

/**
 *
 * @author xmoravc6
 */
public enum Severity {
    Negligible,
    Annoying,
    Distressing,
    Harmful,
    Fatal,
    Cataclysmic,
    Apocalyptic
}
