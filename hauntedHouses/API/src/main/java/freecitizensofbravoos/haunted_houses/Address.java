package freecitizensofbravoos.haunted_houses;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Embeddable;

/**
 *
 * @author xkostoln
 */
@Embeddable
public class Address implements Serializable {
    private String addrState;
    private String addrCity;
    private String addrStreet;
    private String addrNumber;

    public Address() {}  

    public Address(String addrState, String addrCity, String addrStreet, String addrNumber) {
        this.addrState = addrState;
        this.addrCity = addrCity;
        this.addrStreet = addrStreet;
        this.addrNumber = addrNumber;
    }

    public String getAddrState() {
        return addrState;
    }

    public void setAddrState(String addrState) {
        this.addrState = addrState;
    }    
    
    public String getAddrCity() {
        return addrCity;
    }

    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

    public String getAddrStreet() {
        return addrStreet;
    }

    public void setAddrStreet(String addrStreet) {
        this.addrStreet = addrStreet;
    }

    public String getAddrNumber() {
        return addrNumber;
    }

    public void setAddrNumber(String addrNumber) {
        this.addrNumber = addrNumber;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.addrCity);
        hash = 41 * hash + Objects.hashCode(this.addrStreet);
        hash = 41 * hash + Objects.hashCode(this.addrNumber);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Address other = (Address) obj;
        if (!Objects.equals(this.addrCity, other.addrCity)) {
            return false;
        }
        if (!Objects.equals(this.addrStreet, other.addrStreet)) {
            return false;
        }
        if (!Objects.equals(this.addrNumber, other.addrNumber)) {
            return false;
        }
        return true;
    }    
    
    @Override
    public String toString() {
        return "Address{" + "addrCity=" + addrCity + ", addrStreet=" + addrStreet + ", addrNumber=" + addrNumber + '}';
    }    
}
