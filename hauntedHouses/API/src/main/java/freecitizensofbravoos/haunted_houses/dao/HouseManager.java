package freecitizensofbravoos.haunted_houses.dao;

import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import java.util.List;

/**
 *This interface defines CRUD methods for the class House
 * @author xkostoln
 */
public interface HouseManager {
    /**
     * This method creates human, with generated id, since house is entity after creating it will be persisted to database. 
     * @param house House to create. 
     */
    void createHouse(House house);
    
    /**
     * This method returns house with given id.
     * @param id Id of the house to get.
     * @return house with given id.
     */
    public House getHouse(Long id);
    
    /** 
     * This method updates given human. Cannot change the id.
     * @param house House to update.
     */
    void updateHouse(House house);
    
    /**
     * This method deletes house with given id.
     * @param id Id of the house to delete.
     */
    void deleteHouse(Long id);
    
    /**
     * This method returns all houses in the DB.
     * @return all houses in the DB.
     */
    public List<House> getAllHouses();
    
    /**
     * This method returns all humans in house.
     * @param id
     * @return all humans of house
     */
    public List<Human> getAllHumansOfHouse(Long id);
    
    /**
     * This method returns all ghosts in house.
     * @param id
     * @return all ghosts of house
     */
    public List<Ghost> getAllGhostsOfHouse(Long id);
}
