package freecitizensofbravoos.haunted_houses.dao;

import freecitizensofbravoos.haunted_houses.entity.Human;
import java.util.List;

/**
 *This interface defines CRUD methods for the class Human
 * @author xurge
 */
public interface HumanManager {
    /**
     * This method creates human, with generated id, since human is entity after creating it will be persisted to database. 
     * @param human Human to create and put in DB.
     */
    public void createHuman(Human human);
    /**
     * This method returns human with given id.
     * @param id Id of the human to get.
     * @return human with given id
     */
    public Human getHuman(Long id);
    
    /**
     * This method updates given human. Cannot change the id.
     * @param human Human to update
     */
    public void updateHuman(Human human);
    
    /**
     * This method deletes human with given id.
     * @param id Id of the human to delete.
     */
    public void deleteHuman(Long id);
    
    /**
     * This method returns all human, which are in DB. Not only ones in our House.
     * @return List<Human> List of all humans in DB.
     */
    public List<Human> getAllHumans();
}
