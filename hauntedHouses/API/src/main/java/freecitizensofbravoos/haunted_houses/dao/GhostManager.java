package freecitizensofbravoos.haunted_houses.dao;

import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import java.util.List;
import java.util.Set;

/**
 *This interface defines CRUD methods for the class Ghost
 * @author xkaiser1
 */
public interface GhostManager {
    
    /**
     * This method creates ghost, with generated id, since ghost is entity after creating it will be persisted to database.
     * @param ghost Ghost to create.
     */
    void createGhost(Ghost ghost);
    
    /**
     * This method returns ghost with given id.
     * @param id Id of the ghost to get.
     * @return ghost with given id.
     */
    public Ghost getGhost(Long id);
    
    /**
     * This method updates given ghost. Cannot change the id.
     * @param ghost Ghost to update.
     */
    void updateGhost(Ghost ghost);
    
    /**
     * This method deletes ghost with given id.
     * @param id Id of the ghost of the given id.
     */
    void deleteGhost(Long id);
    
    /**
     * This method returns all ghosts in DB.
     * @return all ghost stored in DB.
     */
    public List<Ghost> getAllGhosts();
    
    /**
     * This method returns all the abilities of the ghost with the given id.
     * @param id Id of the ghost.
     * @return abilities of the given ghost.
     */
    public Set<Ability> getAllAbilitiesOfGhost(Long id);
}
