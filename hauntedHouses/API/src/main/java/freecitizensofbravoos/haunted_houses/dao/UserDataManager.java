/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package freecitizensofbravoos.haunted_houses.dao;

import freecitizensofbravoos.haunted_houses.entity.UserData;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface UserDataManager {
    
    public void createUserData(UserData userData);
    
    public UserData getUserData(String login);
    
    public void updateUserData(UserData userData);
    
    public void deleteUserData(String login);
    
    public List<UserData> getAllUserData();
}
