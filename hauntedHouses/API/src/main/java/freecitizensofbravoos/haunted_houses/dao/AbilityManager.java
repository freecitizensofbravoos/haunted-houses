package freecitizensofbravoos.haunted_houses.dao;

import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import java.util.List;

/**
 * This interface defines CRUD methods for the class Ability
 * @author xmoravc6
 */
public interface AbilityManager {
    /**
     * This method creates ability, with generated id, since ability is entity after creating it will be persisted to database.
     * @param ability Ability to create. 
     */
    public void createAbility(Ability ability);
    
    /**
     * This method returns ability with given id. 
     * @param id Id of the ability to get.
     * @return Ability with given id.
     */
    public Ability getAbility(Long id);
    
    /**
     * This method updates given ability. Cannot change the id.
     * @param ability Ability to update. 
     */
    public void updateAbility(Ability ability);
    
    /**
     * This method deletes ability with given Id.
     * @param id Id of the ability to delete.
     */
    public void deleteAbility(Long id);
    
    /**
     * This method returns all abilities in DB.
     * @return all abilities stored in DB.
     */
    public List<Ability> getAbilities();
    
    /**
     * Finds all ghosts with this ability.
     * @param id Id of ability.
     * @return Set of ghosts of given ability.
     */
    public List<Ghost> getAllGhosts(Long id);
}
