package freecitizensofbravoos.servicelayer.service;

import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import java.util.List;

/**
 *
 * @author xurge
 */
public interface HouseService {
    public List<HouseDTO> findAllHouses();
        
    public List<HouseDTO> findHousesByCountry(String country);
    
    public List<GhostDTO> findGhostsOfHouse(HouseDTO house);
    
    public List<HumanDTO> findHumansOfHouse(HouseDTO house);
    
    public void createHouse(HouseDTO house);
    
    public void updateHouse(HouseDTO house);
    
    public void deleteHouse(Long id);
    
    public HouseDTO findHouse(Long id);
}
