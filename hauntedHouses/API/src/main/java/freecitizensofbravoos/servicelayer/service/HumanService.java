package freecitizensofbravoos.servicelayer.service;

import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import java.util.List;

/**
 *
 * @author xurge
 */
public interface HumanService {
    
    public List<HumanDTO> findAllHumans();
    
    public List<HumanDTO> findAllMediums();
        
    public void createHuman(HumanDTO human);
    
    public void updateHuman(HumanDTO human);
    
    public void deleteHuman(Long id);
    
    public HumanDTO findHuman(Long id);
}
