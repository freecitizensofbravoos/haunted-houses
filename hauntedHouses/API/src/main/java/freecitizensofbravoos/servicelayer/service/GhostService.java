package freecitizensofbravoos.servicelayer.service;

import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import java.util.List;

/**
 *
 * @author xurge
 */
public interface GhostService {
    
    public List<GhostDTO> findAllGhosts();
            
    public List<AbilityDTO> findAbilitiesOfGhost(Long id);
    
    public void createGhost(GhostDTO ghost);
    
    public void updateGhost(GhostDTO ghost);
    
    public void deleteGhost(Long id);
    
    public GhostDTO findGhost(Long id);
    
    public List<GhostDTO> findGhostsByName(String name);
}
