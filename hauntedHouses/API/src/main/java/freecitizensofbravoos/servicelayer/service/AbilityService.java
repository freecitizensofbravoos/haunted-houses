package freecitizensofbravoos.servicelayer.service;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import java.util.List;

/**
 *
 * @author xurge
 */
public interface AbilityService {
    
    public List<AbilityDTO> findAllAbilities();
    
    public List<AbilityDTO> findAbilitiesBySeverity(Severity severity);
        
    public void createAbility(AbilityDTO ability);
    
    public void updateAbility(AbilityDTO ability);
    
    public void deleteAbility(Long id);
    
    public AbilityDTO findAbility(Long id);
}
