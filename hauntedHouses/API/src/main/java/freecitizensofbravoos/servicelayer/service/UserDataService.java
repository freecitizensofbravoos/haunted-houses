package freecitizensofbravoos.servicelayer.service;

import freecitizensofbravoos.servicelayer.dto.UserDataDTO;
import java.util.List;

/**
 *
 * @author Matus
 */
public interface UserDataService {
    
    public void createUserData(UserDataDTO userDataDTO);
    
    public UserDataDTO findUserData(String login);
    
    public void updateUserDataExceptPassword(UserDataDTO userDataDTO);
    
    public void updateUserPassword(String login, String newPassword);
    
    public void deleteUserData(String login);
    
    public List<UserDataDTO> getAllUserData();
}
