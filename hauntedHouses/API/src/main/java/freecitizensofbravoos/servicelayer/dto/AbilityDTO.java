package freecitizensofbravoos.servicelayer.dto;

import freecitizensofbravoos.haunted_houses.Severity;
import java.util.Objects;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author xmoravc6
 */
public class AbilityDTO {
    private Long id; 
    
    @Pattern(regexp = "^\\S(\\s|\\w|\\d)*?$")
    @NotEmpty
    @Size(max = 30)
    private String name;     
    
    @Pattern(regexp = "^(\\s|\\w|\\d|\\.|\\,|\\!|\\?|\\-)*?$")
    @Size(max = 255)
    private String description; 
    
    private Severity severity;

    public AbilityDTO(Long id, String name, String description, Severity severity)
    {
        this.id = id;
        this.name = name;
        this.description = description;
        this.severity = severity;
    }
    
    public AbilityDTO() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        hash = 43 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 43 * hash + (this.description != null ? this.description.hashCode() : 0);
        hash = 43 * hash + (this.severity != null ? this.severity.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final AbilityDTO other = (AbilityDTO) obj;
        if (!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }
        if ((this.description == null) ? (other.description != null) : !this.description.equals(other.description)) {
            return false;
        }
        return this.severity == other.severity;
    }    
}
