package freecitizensofbravoos.servicelayer.dto;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author andris
 */
public class GhostDateSerializer extends JsonSerializer<Date> {
    
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    
    @Override
    public void serialize(Date value, JsonGenerator gen, SerializerProvider provider) 
            throws IOException, JsonProcessingException {
        String formattedDate = dateFormat.format(value);
        gen.writeString(formattedDate);
    }
}
