package freecitizensofbravoos.servicelayer.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author kika
 */
public class GhostDTO {   
    
    private Long id;
    
    @Pattern(regexp = "^\\S(\\s|\\w|\\d)*?$")
    @NotEmpty
    @Size(max = 30)
    private String name;
    
    @DateTimeFormat(pattern = "HH:mm")
    @JsonSerialize(using = GhostDateSerializer.class)
    private Date beginningOfHaunt;
    
    @DateTimeFormat(pattern = "HH:mm")
    @JsonSerialize(using = GhostDateSerializer.class)
    private Date endOfHaunt;   
    
    @Min(0)
    private int energyToHaunt;   
    
    @Min(0)
    private int numberOfPeopleHaunted;
    
    private Set<AbilityDTO> abilities = new HashSet<>();
    
    private HouseDTO house;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginningOfHaunt() {
        return beginningOfHaunt;
    }

    public void setBeginningOfHaunt(Date beginningOfHaunt) {
        this.beginningOfHaunt = beginningOfHaunt;
    }

    public Date getEndOfHaunt() {
        return endOfHaunt;
    }

    public void setEndOfHaunt(Date endOfHaunt) {
        this.endOfHaunt = endOfHaunt;
    }

    public int getEnergyToHaunt() {
        return energyToHaunt;
    }

    public void setEnergyToHaunt(int energyToHaunt) {
        this.energyToHaunt = energyToHaunt;
    }

    public int getNumberOfPeopleHaunted() {
        return numberOfPeopleHaunted;
    }

    public void setNumberOfPeopleHaunted(int numberOfPeopleHaunted) {
        this.numberOfPeopleHaunted = numberOfPeopleHaunted;
    }

    public Set<AbilityDTO> getAbilities() {
        return abilities;
    }

    public void setAbilities(Set<AbilityDTO> abilities) {
        this.abilities = abilities;
    }

    public HouseDTO getHouse() {
        return house;
    }

    public void setHouse(HouseDTO house) {
        this.house = house;
    }

    public boolean addAbility(AbilityDTO ability) {
        if(ability == null) {
            throw new IllegalArgumentException("Ability cannot be null!");
        }        
        return abilities.add(ability);
    }
    
    public boolean removeAbility(AbilityDTO ability) {
        return abilities.remove(ability);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GhostDTO other = (GhostDTO) obj;
        return !(!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return "GhostDTO{" + "id=" + id + ", name=" + name + ", beginningOfHaunt=" + beginningOfHaunt + ", endOfHaunt=" + endOfHaunt + ", energyToHaunt=" + energyToHaunt + ", numberOfPeopleHaunted=" + numberOfPeopleHaunted + ", abilities=" + abilities + '}';
    }
}
