package freecitizensofbravoos.servicelayer.dto;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author xurge
 */
@JsonAutoDetect
public class HumanDTO {
    private Long id;

    @Pattern(regexp = "^\\S(\\s|\\w|\\d)*?$")
    @NotEmpty
    @Size(max = 30)
    private String name;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")    
    @JsonSerialize(using = HumanDateSerializer.class)
    private Date dateOfBirth;
    
    @Pattern(regexp = "^(\\s|\\w|\\d|\\.|\\,|\\!|\\?|\\-)*?$")
    @Size(max = 30)
    private String afraidOf;
    
    private boolean medium;
    
    private HouseDTO house;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAfraidOf() {
        return afraidOf;
    }

    public void setAfraidOf(String afraidOf) {
        this.afraidOf = afraidOf;
    }

    public boolean isMedium() {
        return medium;
    }

    public void setMedium(boolean medium) {
        this.medium = medium;
    }

    public HouseDTO getHouse() {
        return house;
    }

    public void setHouse(HouseDTO house) {
        this.house = house;
    }    

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HumanDTO other = (HumanDTO) obj;
        return !(!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return "Id=" + id + ", Name=" + name + ", Date of birth=" + dateFormat.format(dateOfBirth) + 
                ", Afraid of=" + afraidOf + ", Medium=" + medium;         
    }
}
