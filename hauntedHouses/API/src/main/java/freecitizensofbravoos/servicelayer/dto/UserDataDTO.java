package freecitizensofbravoos.servicelayer.dto;

import java.util.Objects;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Matus
 */
public class UserDataDTO {
    @Pattern(regexp = "^\\S(\\s|\\w|\\d)*?$")
    @NotEmpty
    @Size(min = 3, max = 30)
    private String username;
    
    @NotEmpty
    @Size(min = 6, max = 30)
    private String password;
    
    private String authorities;
    
    private boolean enabled; 

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String authorities) {
        this.authorities = authorities;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.username);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserDataDTO other = (UserDataDTO) obj;
        if (!Objects.equals(this.username, other.username)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserDataDTO{" + "login=" + username + ", password=" + password + ", authorities=" + authorities + ", enabled=" + enabled + '}';
    }
}
