package freecitizensofbravoos.servicelayer.dto;

import freecitizensofbravoos.haunted_houses.Address;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author andris
 */
public class HouseDTO {
    
    private Long id;
    
    @Pattern(regexp = "^\\S(\\s|\\w|\\d)*?$")
    @NotEmpty
    @Size(max = 30)
    private String name;
    
    private Address address;
    
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date startOfHaunting;
    
    @Pattern(regexp = "^(\\s|\\w|\\d|\\.|\\,|\\!|\\?|\\-)*?$")
    @Size(max = 255)
    private String history;
    
    private List<HumanDTO> humans = new ArrayList<>();
    private List<GhostDTO> ghosts = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getStartOfHaunting() {
        return startOfHaunting;
    }

    public void setStartOfHaunting(Date startOfHaunting) {
        this.startOfHaunting = startOfHaunting;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public List<HumanDTO> getHumans() {
        return humans;
    }

    public void setHumans(List<HumanDTO> humans) {
        this.humans = humans;
    }

    public List<GhostDTO> getGhosts() {
        return ghosts;
    }

    public void setGhosts(List<GhostDTO> ghosts) {
        this.ghosts = ghosts;
    }   
    
    public void addHuman(HumanDTO human) {
        if(human == null) {
            throw new IllegalArgumentException("Human cannot be null!");
        }        
        humans.add(human);
        human.setHouse(this);
    }

    public void removeHuman(HumanDTO human) {
        humans.remove(human); 
        human.setHouse(null);
    }

    public void addGhost(GhostDTO ghost) {
        if(ghost == null) {
            throw new IllegalArgumentException("Ghost cannot be null!");
        }
        ghosts.add(ghost);
        ghost.setHouse(this);
    }

    public void removeGhost(GhostDTO ghost) {
        ghosts.remove(ghost);
        ghost.setHouse(null);
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HouseDTO other = (HouseDTO) obj;
        return !(!Objects.equals(this.id, other.id) && (this.id == null || !this.id.equals(other.id)));
    }
}
