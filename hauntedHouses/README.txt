The project can be built by running the following command:

	mvn clean install

------------------------------------------------------------------------------------

To start the web server you need to run the next command from the PresentationLayer folder:

	mvn tomcat7:run

To run the server successfully it is required to have a DB machine listening on port 1527 on the localhost with database name, username and password all set to 'pa165'.

------------------------------------------------------------------------------------

To start the REST client you need to run the next command from the RESTClient folder:

	mvn exec:java
