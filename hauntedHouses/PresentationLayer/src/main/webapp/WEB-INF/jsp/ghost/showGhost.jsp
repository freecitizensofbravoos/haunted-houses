<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="ghost.showGhost"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="ghostNavigationForm.jsp"%>  
    <div id="framecontent">
        <div id="chosen">
            <table id="chosenTable">
                <tr>
                    <td colspan="2"><h2><fmt:message key="ghost.list.name"/> : ${ghost.name}</h2></td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ghost.list.hauntBegin"/> : </b><fmt:formatDate pattern="HH:mm" value="${ghost.beginningOfHaunt}"/></td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ghost.list.hauntEnd"/> : </b><fmt:formatDate pattern="HH:mm" value="${ghost.endOfHaunt}"/></td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ghost.list.energy"/> : </b>${ghost.energyToHaunt}</td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ghost.list.peopleHaunted"/> : </b>${ghost.numberOfPeopleHaunted}</td>
                </tr>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <td>
                    <form method="get" action="${pageContext.request.contextPath}/ghost/update/${ghost.id}">
                        <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/ghost/ghostList/delete" method="post" commandName="deleteGhost">
                        <input type="hidden" id="ghostId" name="ghostId" value=${ghost.id}>
                        <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                    </form>
                </td>
                </sec:authorize>
             </table>
         </div>
         <div id="humans">
            <h2> <fmt:message key="ghost.abilities"/></h2>
            <table class="listTable">
                <tr>
                    <th><fmt:message key="ability.list.name"/></th>
                    <th><fmt:message key="ability.list.severity"/></th>
                </tr>
                <c:forEach items="${abilities}" var="ability">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/ability/showAbility/${ability.id}">${ability.name}</a></td>
                        <td>${ability.severity}</td>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                        <form action="${pageContext.request.contextPath}/ability/removeAbilityFromGhost" method="post">
                            <input type="hidden" name="abilityId" value=${ability.id}>
                            <input type="hidden" name="ghostId" value=${ghost.id}>
                            <input id="generalButton" type="submit" value="<fmt:message key='admin.button.removeFromGhost'/>">
                        </form>
                        </td> 
                        </sec:authorize>
                    </tr>
                </c:forEach>
            </table>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
            <form method="get" action="${pageContext.request.contextPath}/ability/addAbilityList">
                <input type="hidden" name="ghostId" value="${ghost.id}">
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.addAbility'/>">
            </form>    
            </sec:authorize>
         </div>           
    </div>  
</jsp:attribute>
</my:layout>
