<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="../house/houseNavigationForm.jsp"%>   
    <div id="framecontent">
        <div id="ghostFilter">
            <form method="get" action="${pageContext.request.contextPath}/ghost/ghostList">
                <input type="text" name="name" value="${filterValue}">
                <input type="submit" value="<fmt:message key='admin.button.find'/>">
            </form>
        </div>
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="ghost.list.name"/></th>
                <th><fmt:message key="ghost.list.hauntBegin"/></th>
                <th><fmt:message key="ghost.list.hauntEnd"/></th>
                <th><fmt:message key="ghost.list.energy"/></th>
                <th><fmt:message key="ghost.list.peopleHaunted"/></th>                        
             </tr>
            <c:forEach items="${homelessGhosts}" var="ghost">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/ghost/showGhost/${ghost.id}">${ghost.name}</a></td>
                    <td><fmt:formatDate pattern="HH:mm" value="${ghost.beginningOfHaunt}"/></td>
                    <td><fmt:formatDate pattern="HH:mm" value="${ghost.endOfHaunt}"/></td>
                    <td>${ghost.energyToHaunt}</td>
                    <td>${ghost.numberOfPeopleHaunted}</td>
                    <td>
                        <form action="addGhostList" method="post">
                            <input type="hidden" name="ghostId" value=${ghost.id}>
                            <input type="hidden" name="houseId" value=${houseId}>
                            <input type="submit" value=<fmt:message key="admin.button.add"/>>                           
                        </form>
                    </td>  
                </tr>
            </c:forEach>
         </table>
    </div>     
</jsp:attribute>
</my:layout>
   
        
    
