<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="ghostNavigationForm.jsp"%>  
    <div id="framecontent">
        <div id="filter">
            <form method="get" action="${pageContext.request.contextPath}/ghost/ghostList">
                <b><fmt:message key='ghost.filter.text'/></b>
                <input type="text" name="name" value="${filterValue}">
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.find'/>">
            </form>
        </div>
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="ghost.list.name"/></th>
                <th><fmt:message key="ghost.list.hauntBegin"/></th>
                <th><fmt:message key="ghost.list.hauntEnd"/></th>
                <th><fmt:message key="ghost.list.energy"/></th>
                <th><fmt:message key="ghost.list.peopleHaunted"/></th>                        
             </tr>
            <c:forEach items="${ghostList}" var="ghost">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/ghost/showGhost/${ghost.id}">${ghost.name}</a></td>
                    <td><fmt:formatDate pattern="HH:mm" value="${ghost.beginningOfHaunt}"/></td>
                    <td><fmt:formatDate pattern="HH:mm" value="${ghost.endOfHaunt}"/></td>
                    <td>${ghost.energyToHaunt}</td>
                    <td>${ghost.numberOfPeopleHaunted}</td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/ghost/update/${ghost.id}">
                                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/ghost/ghostList/delete" method="post" commandName="deleteGhost">
                                <input type="hidden" id="ghostId" name="ghostId" value=${ghost.id}>
                                <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                            </form>
                        </td>
                    </sec:authorize>
                </tr>
            </c:forEach>
         </table>
    </div> 
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <div id="createButton">
            <form method="get" action="${pageContext.request.contextPath}/ghost/createGhost">
                <input type="hidden" name="houseId" value=-1>
                <input id="createButton" type="submit" value="<fmt:message key='admin.button.create.ghost'/>">
            </form>
        </div>
    </sec:authorize>
</jsp:attribute>
</my:layout>
   
        
    
