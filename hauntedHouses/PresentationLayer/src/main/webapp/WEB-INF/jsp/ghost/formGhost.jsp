<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

 <table border="0">
    <tr>
        <td><b><fmt:message key="ghost.list.name"/></b></td>
        <td><form:input path="name" required="true" htmlEscape="true"/></td>
        <td><form:errors path="name" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ghost.list.hauntBegin"/></b></td>
        <td><form:input type= "datetime" path="beginningOfHaunt" placeholder="HH:mm" htmlEscape="true"/></td>
        <td><form:errors path="beginningOfHaunt" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ghost.list.hauntEnd"/></b></td>
        <td><form:input type= "datetime" path="endOfHaunt" placeholder="HH:mm" htmlEscape="true"/></td>
        <td><form:errors path="endOfHaunt" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ghost.list.energy"/></b></td>
        <td><form:input type="number" path="energyToHaunt" htmlEscape="true"/></td>
        <td><form:errors path="energyToHaunt" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ghost.list.peopleHaunted"/></b></td>
        <td><form:input type="number" path="numberOfPeopleHaunted" htmlEscape="true"/></td>
        <td><form:errors path="numberOfPeopleHaunted" cssClass="error"/></td>
    </tr>
</table>
