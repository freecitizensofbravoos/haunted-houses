<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   

<fmt:message var="title" key="ghost.createGhost"/>
<my:layout title="${title}">
<jsp:attribute name="body">
<body>
    <%@include file="ghostNavigationForm.jsp"%>
    <div id="newContent">
        <form:form action="createGhost" method="post" commandName="createGhostForm">
            <%@include file="formGhost.jsp"%>
            <input type="hidden" name="houseId" value="${houseId}">
            <input type="submit" id="confirmButton" value="<fmt:message key='admin.button.create'/>" />
        </form:form>
    </div>
</jsp:attribute>
</my:layout>