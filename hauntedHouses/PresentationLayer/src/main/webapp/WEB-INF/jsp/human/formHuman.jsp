<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table border="0">    
    <tr>
        <td><b><fmt:message key="human.list.name"/></b></td>
        <td><form:input path="name" required="true" htmlEscape="true"/></td>
        <td><form:errors path="name" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="human.list.birth"/></b></td>
        <td><form:input path="dateOfBirth" placeholder="dd/MM/yyyy" htmlEscape="true"/></td>
        <td><form:errors path="dateOfBirth" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="human.list.afraidOf"/></b></td>
        <td><form:input path="afraidOf" htmlEscape="true"/></td>
        <td><form:errors path="afraidOf" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="human.list.medium"/></b></td>
        <td><form:checkbox label="Yes" path="medium" htmlEscape="true"/></td>
        <td><form:errors path="medium" cssClass="error"/></td>
    </tr>
</table>
