<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="humanNavigationForm.jsp"%>
    <div id="framecontent">
        <div id="filter">
            <form method="get" action="${pageContext.request.contextPath}/human/humanList">
                <b><fmt:message key='human.filter.text'/></b>
                <select id="typeSelect" name="type">
                    <option value="all"><fmt:message key='field.all'/></option>
                    <option value="medium"><fmt:message key='human.medium'/></option>
                </select>                
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.find'/>">                
            </form>
        </div>
        <script>                
            document.getElementById('typeSelect').value = "${filterValue}";
        </script>
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="human.list.name"/></th>
                <th><fmt:message key="human.list.birth"/></th>
                <th><fmt:message key="human.list.afraidOf"/></th>
                <th><fmt:message key="human.list.medium"/></th>
             </tr>        
            <c:forEach items="${humans}" var="human">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/human/showHuman/${human.id}">${human.name}</a></td>
                    <td><fmt:formatDate type="date" value="${human.dateOfBirth}"/></td>
                    <td>${human.afraidOf}</td>                        
                        <c:choose>
                            <c:when test = "${human.medium}">     
                                <td><fmt:message key="human.list.medium.yes"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><fmt:message key="human.list.medium.no"/></td>
                            </c:otherwise>
                        </c:choose>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/human/update/${human.id}">
                                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/human/humanList/delete" method="post">
                                <input type="hidden" name="humanId" value=${human.id}>
                                <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                            </form>
                        </td>
                    </sec:authorize>
                </tr>
            </c:forEach>
         </table>
    </div> 
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <div id="createButton">
            <form method="get" action="${pageContext.request.contextPath}/human/createHuman">
                <input type="hidden" name="houseId" value=-1>
                <input id="createButton" type="submit" value="<fmt:message key='admin.button.create.human'/>">
            </form>
        </div>  
    </sec:authorize>
</jsp:attribute>
</my:layout>
