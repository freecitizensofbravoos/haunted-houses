<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="../house/houseNavigationForm.jsp"%>
    <div id="framecontent">
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="human.list.name"/></th>
                <th><fmt:message key="human.list.birth"/></th>
                <th><fmt:message key="human.list.afraidOf"/></th>
                <th><fmt:message key="human.list.medium"/></th>
             </tr>        
            <c:forEach items="${homelessHumans}" var="human">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/human/showHuman/${human.id}">${human.name}</a></td>
                    <td><fmt:formatDate type="date" value="${human.dateOfBirth}"/></td>
                    <td>${human.afraidOf}</td>                        
                    <c:choose>
                        <c:when test = "${human.medium}">     
                            <td><fmt:message key="human.list.medium.yes"/></td>
                        </c:when>
                        <c:otherwise>
                            <td><fmt:message key="human.list.medium.no"/></td>
                        </c:otherwise>
                    </c:choose>
                    <td>
                        <form action="addHumanList" method="post">
                            <input type="hidden" name="humanId" value=${human.id}>
                            <input type="hidden" name="houseId" value=${houseId}>
                            <input type="submit" value=<fmt:message key="admin.button.add"/>>                           
                        </form>
                    </td>      
                </tr>
            </c:forEach>
         </table>
    </div>     
</jsp:attribute>
</my:layout>
