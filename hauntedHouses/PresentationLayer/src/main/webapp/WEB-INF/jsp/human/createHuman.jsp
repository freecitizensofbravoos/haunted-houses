<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<fmt:message var="title" key="human.createHuman"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="humanNavigationForm.jsp"%>
    <div id="newContent">
        <form:form action="createHuman" method="post" commandName="createHumanForm">
            <%@include file="formHuman.jsp"%>
            <input type="hidden" name="houseId" value="${houseId}">
            <input type="submit" id="confirmButton" value="<fmt:message key='admin.button.create'/>" />
            
        </form:form>
    </div>
</jsp:attribute>
</my:layout>