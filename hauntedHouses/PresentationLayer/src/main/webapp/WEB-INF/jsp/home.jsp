<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')">
        <div id="categories">
            <li class="active"><a href="${pageContext.request.contextPath}/"><fmt:message key="navigation.index"/></a></li> 
            <li><a href="${pageContext.request.contextPath}/house/houseList"><fmt:message key="navigation.houses"/></a></li>
            <li><a href="${pageContext.request.contextPath}/ghost/ghostList"/><fmt:message key="navigation.ghosts"/></a></li>
            <li><a href="${pageContext.request.contextPath}/human/humanList"/><fmt:message key="navigation.humans"/></a></li>
            <li><a href="${pageContext.request.contextPath}/ability/abilityList"><fmt:message key="navigation.abilities"/></a></li>
        </div>
    </sec:authorize>
        <div id="homecontent">
            <div id="img">
                <img src="images/ghost.png">
            </div>
            <div id="img">
                <img src="images/ghost.png">
            </div>
            <div id="home1">
                <h1><fmt:message key="home.title.one"/></h1>
                <ul class="a">
                    <li><h3><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"><a href="${pageContext.request.contextPath}/house/houseList"></sec:authorize><fmt:message key="home.title.house.one"/><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"></a></sec:authorize></h3></li>
                    <li><h3><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"><a href="${pageContext.request.contextPath}/human/humanList"></sec:authorize><fmt:message key="home.title.human"/><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"></a></sec:authorize></h3></li>
                </ul>
            </div>
            <div id="home2">
                <h1><fmt:message key="home.title.two"/></h1>
                <ul class="a">
                    <li><h3><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"><a href="${pageContext.request.contextPath}/house/houseList"></sec:authorize><fmt:message key="home.title.house.two"/><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"></a></sec:authorize></h3></li>
                    <li><h3><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"><a href="${pageContext.request.contextPath}/ghost/ghostList"></sec:authorize><fmt:message key="home.title.ghost"/><sec:authorize access="hasAnyRole('ROLE_ADMIN','ROLE_USER')"></a></sec:authorize></h3></li>
                </ul>
            </div>
        </div>
</jsp:attribute>
</my:layout>
