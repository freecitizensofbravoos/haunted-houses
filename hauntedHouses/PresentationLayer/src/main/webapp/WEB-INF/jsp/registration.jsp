<%-- 
    Document   : registration
    Created on : 21.11.2014, 12:22:49
    Author     : kika
--%>

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%> 
<html>
<head>
<title><fmt:message key="registration.title"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css"/>
</head>
<body>
    <div id="header">
            <div class="title"><fmt:message key="registration.title"/></div>            

            <div class="language">
                <th>
                <a href="?lang=en"><img src="${pageContext.request.contextPath}/images/english.png" alt="en" title="English"></a>
                <a href="?lang=sk"><img src="${pageContext.request.contextPath}/images/slovak.png" alt="sk" title="Slovensky"></a>
                <a href="?lang=cs"><img src="${pageContext.request.contextPath}/images/czech.png" alt="cz" title="Česky"></a>
                </th>
                <th>
                    <div id="backHome">
                        <a href="${pageContext.request.contextPath}"><fmt:message key="login.home"/></a>
                    </div>
                </th>
            </div>
    </div>
    <div id="newContent">
        <c:if test="${not empty usedError}">
            <div class="msg"><fmt:message key="registration.msg"/></div>
        </c:if>
        <form:form action="register" method="post" commandName="userForm">
            <table border="0">
                <tr>
                    <td><fmt:message key="registration.login"/></td>
                    <td><form:input path="username" required="true" htmlEscape="true"/></td>
                    <td><form:errors path="username" cssClass="error"/></td>
                </tr>
                <tr>
                    <td><fmt:message key="login.password"/></td>
                    <td><form:password path="password" required="true"/></td>
                    <td><form:errors path="password" cssClass="error"/></td>
                </tr>                
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Register" /></td>
                </tr>
            </table>
        </form:form>
    </div>
    
</body>
</html>
