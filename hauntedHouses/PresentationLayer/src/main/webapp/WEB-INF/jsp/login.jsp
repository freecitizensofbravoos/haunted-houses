<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<title><fmt:message key="login.page"/></title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css"/>

</head>
<body onload='document.loginForm.username.focus();'>
    <div id="header">
            <div class="title"><fmt:message key="login.title"/></div>            

            <div class="language">
                <th>
                <a href="?lang=en"><img src="${pageContext.request.contextPath}/images/english.png" alt="en" title="English"></a>
                <a href="?lang=sk"><img src="${pageContext.request.contextPath}/images/slovak.png" alt="sk" title="Slovensky"></a>
                <a href="?lang=cs"><img src="${pageContext.request.contextPath}/images/czech.png" alt="cz" title="Česky"></a>
                </th>
                <th>
                    <div id="backHome">
                        <a href="${pageContext.request.contextPath}"><fmt:message key="login.home"/></a>
                    </div>
                </th>
            </div>
    </div>
	<div id="newContent">
 
		<c:if test="${not empty error}">
			<div class="error"><fmt:message key="login.error"/></div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg"><fmt:message key="login.msg"/></div>
		</c:if>
 
		<form name='loginForm'
		  action="<c:url value='j_spring_security_check' />" method='POST'>
 
		  <table>
			<tr>
				<td><fmt:message key="login.login"/></td>
				<td><input type='text' name='username' value=''></td>
			</tr>
			<tr>
				<td><fmt:message key="login.password"/></td>
				<td><input type='password' name='password' /></td>
			</tr>
			<tr>
				<td colspan='2'><input name="submit" type="submit"
					value="<fmt:message key="registrationMenu.login"/>"</td>
			</tr>
		  </table>
 
		  <input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
 
		</form>
	</div>
        
</body>
</html>
