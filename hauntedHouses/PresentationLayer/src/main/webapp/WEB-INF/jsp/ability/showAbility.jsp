<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="ability.showAbility"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="abilityNavigationForm.jsp"%>
    <div id="framecontent">
        <div id="chosen">
            <table id="chosenTable">
                <tr>
                    <td colspan="2"><h2><fmt:message key="ability.list.name"/> : ${ability.name}</h2></td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ability.list.description"/> : </b>${ability.description}</td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="ability.list.severity"/> : </b>${ability.severity}</td>
                </tr>  
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <td>
                    <form method="get" action="${pageContext.request.contextPath}/ability/update/${ability.id}">
                        <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                    </form>
                </td>
                <td>
                    <form action="${pageContext.request.contextPath}/ability/abilityList/delete" method="post">
                        <input type="hidden" name="abilityId" value=${ability.id}>
                        <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                    </form>
                </td> 
                </sec:authorize>
             </table>
         </div>
    </div>  
</jsp:attribute>
</my:layout>
