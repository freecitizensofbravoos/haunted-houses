<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table border="0">
    <tr>
        <td><b><fmt:message key="ability.list.name"/></b></td>
        <td><form:input path="name" required="true" htmlEscape="true"/></td>        
        <td><form:errors path="name" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ability.list.description"/></b></td>        
        <td><form:textarea path="description" htmlEscape="true"/></td>
        <td><form:errors path="description" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="ability.list.severity"/></b></td>
        <td>
            <form:select path="severity">
                <c:forEach items="${severities}" var="severity">   
                    <form:option id="${severity}ListOption" value="${severity}"><fmt:message key="severity.${severity}"/></form:option>>
                </c:forEach>
            </form:select>            
        </td>
        <td><form:errors path="severity" cssClass="error"/></td>
    </tr>
</table>

