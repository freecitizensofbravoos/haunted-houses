<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   

<fmt:message var="title" key="ability.createAbility"/>
<my:layout title="${title}">
<jsp:attribute name="body">
<body>
    <%@include file="abilityNavigationForm.jsp"%>
    <div id="newContent">
        <form:form action="createAbility" method="post" commandName="createAbilityForm">
            <%@include file="formAbility.jsp"%>
            <input type="hidden" name="ghostId" value="${ghostId}">
            <input id="confirmButton" type="submit" value="<fmt:message key='admin.button.create'/>" />
        </form:form>
    </div>
</jsp:attribute>
</my:layout>