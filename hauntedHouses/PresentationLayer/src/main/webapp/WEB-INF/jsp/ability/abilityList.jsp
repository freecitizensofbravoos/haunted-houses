<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="abilityNavigationForm.jsp"%>
    <div id="framecontent">
        <div id="filter">
            <form method="get" action="${pageContext.request.contextPath}/ability/abilityList">
                <b><fmt:message key='ability.filter.text'/></b>
                <select id="severitySelect" name="severity">
                    <option value="all"><fmt:message key='field.all'/></option>
                    <c:forEach items="${severities}" var="severity">                           
                        <option value="${severity}"><fmt:message key="severity.${severity}"/></option>
                    </c:forEach>                    
                </select>                
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.find'/>">
            </form>
        </div>
        <script>                
            document.getElementById('severitySelect').value = "${filterValue}";
        </script>
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="ability.list.name"/></th>
                <th><fmt:message key="ability.list.description"/></th>
                <th><fmt:message key="ability.list.severity"/></th>
             </tr>
            <c:forEach items="${abilities}" var="ability">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/ability/showAbility/${ability.id}">${ability.name}</a></td>
                    <td id="abilityDescriptionFrame">${ability.description}</td>
                    <td><fmt:message key="severity.${ability.severity}"/></td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/ability/update/${ability.id}">                            
                                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/ability/abilityList/delete" method="post">
                                <input type="hidden" name="abilityId" value=${ability.id}>
                                <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                            </form>
                        </td>
                    </sec:authorize>
                </tr>
            </c:forEach>
         </table>
    </div>  
    <sec:authorize access="hasRole('ROLE_ADMIN')">
            <div id="createButton">
                    <form method="get" action="${pageContext.request.contextPath}/ability/createAbility"  >
                        <input type="hidden" name="ghostId" value=-1>
                        <input id="createButton" type="submit" value="<fmt:message key='admin.button.create.ability'/>">
                    </form>
            </div> 
    </sec:authorize>
</jsp:attribute>
</my:layout>
