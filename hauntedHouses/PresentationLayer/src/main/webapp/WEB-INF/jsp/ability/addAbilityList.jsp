<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="../ghost/ghostNavigationForm.jsp"%>
    <div id="framecontent">
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="ability.list.name"/></th>
                <th><fmt:message key="ability.list.description"/></th>
                <th><fmt:message key="ability.list.severity"/></th>
             </tr>
            <c:forEach items="${remainingAbilities}" var="ability">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/ability/showAbility/${ability.id}">${ability.name}</a></td>
                    <td><div id="abilityDescriptionFrame">${ability.description}</div></td>
                    <td><fmt:message key="severity.${ability.severity}"/></td>                    
                    
                    <td>
                        <form action="addAbilityList" method="post">
                            <input type="hidden" name="abilityId" value=${ability.id}>
                            <input type="hidden" name="ghostId" value=${ghostId}>
                            <input type="submit" value=<fmt:message key="admin.button.add"/>>                           
                        </form>
                    </td>  
                </tr>
            </c:forEach> 
            <c:forEach items="${ghostAbilities}" var="ability">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/ability/showAbility/${ability.id}">${ability.name}</a></td>
                    <td><div id="abilityDescriptionFrame">${ability.description}</div></td>
                    <td><fmt:message key="severity.${ability.severity}"/></td>
                </tr>
            </c:forEach>
         </table>
    </div>
</jsp:attribute>
</my:layout>
