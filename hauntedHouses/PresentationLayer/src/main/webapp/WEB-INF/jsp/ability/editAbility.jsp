<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<fmt:message var="title" key="ability.editAbility"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="abilityNavigationForm.jsp"%>
    <div id="newContent">
        <form:form method="post" action="${pageContext.request.contextPath}/ability/update" modelAttribute="ability">
            <form:hidden path="id"/>
            <%@include file="formAbility.jsp"%>
            <input type="hidden" name="ghostId" value="${ghostId}">
            <input type="submit" id="confirmButton" value="<fmt:message key='admin.button.edit'/>"/>
        </form:form>
    </div>
</jsp:attribute>
</my:layout>
