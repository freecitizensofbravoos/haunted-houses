<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>   

<fmt:message var="title" key="house.createHouse"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="houseNavigationForm.jsp"%>
    <div id="newContent">
        <form:form action="createHouse" method="post" commandName="createHouseForm">        
            <%@include file="formHouse.jsp"%>
            <input type="submit" id="confirmButton" value="<fmt:message key='admin.button.create'/>" />
        </form:form>
    </div>
</jsp:attribute>
</my:layout>