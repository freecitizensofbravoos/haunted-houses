<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div id="categories">
    <li id="home"><a href="${pageContext.request.contextPath}/"><fmt:message key="navigation.index"/></a></li> 
    <li id="houses" class="active"><a href="${pageContext.request.contextPath}/house/houseList"><fmt:message key="navigation.houses"/></a></li>
    <li id="ghosts"><a href="${pageContext.request.contextPath}/ghost/ghostList"/><fmt:message key="navigation.ghosts"/></a></li>
    <li id="humans"><a href="${pageContext.request.contextPath}/human/humanList"/><fmt:message key="navigation.humans"/></a></li>
    <li id="abilities"><a href="${pageContext.request.contextPath}/ability/abilityList"><fmt:message key="navigation.abilities"/></a></li>
</div>
