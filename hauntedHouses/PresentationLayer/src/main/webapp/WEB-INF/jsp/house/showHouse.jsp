<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="house.showHouse"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="houseNavigationForm.jsp"%>
    <div id="framecontent">
        <div id="chosen">
            <table id="chosenTable">
                <tr>
                    <td colspan="2">
                        <h2><fmt:message key="house.list.name"/> : ${house.name}</h2>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="house.list.street"/> : </b>${house.address.addrStreet}</td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="house.list.city"/> : </b>${house.address.addrCity}</td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="house.list.state"/> : </b>${house.address.addrState}</td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="house.list.hauntStarted"/> : </b><fmt:formatDate type="date" value="${house.startOfHaunting}"/></td>
                </tr>
                <tr>
                    <td colspan="2"><b><fmt:message key="house.list.history"/> : </b>${house.history}</td>
                </tr>
                <sec:authorize access="hasRole('ROLE_ADMIN')">
                <tr>
                    <td>
                        <form method="get" action="${pageContext.request.contextPath}/house/update/${house.id}">
                            <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                        </form>
                     </td>
                     <td>
                        <form action="${pageContext.request.contextPath}/house/houseList/delete" method="post">
                            <input type="hidden" name="houseId" value=${house.id}>
                            <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                        </form>
                    </td>
                </tr>
                </sec:authorize>
            </table>
        </div>
        <div id="humans">
            <h2> <fmt:message key="house.humans"/></h2>
            <table class="listTable">
                <tr>
                    <th><fmt:message key="human.list.name"/></th>
                    <th><fmt:message key="human.list.afraidOf"/></th>
                    <th><fmt:message key="human.list.medium"/></th>
                </tr>
                <c:forEach items="${humansInHouse}" var="human">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/human/showHuman/${human.id}">${human.name}</a></td>
                        <td>${human.afraidOf}</td>
                        <c:choose>
                            <c:when test = "${human.medium}">     
                                <td><fmt:message key="human.list.medium.yes"/></td>
                            </c:when>
                            <c:otherwise>
                                <td><fmt:message key="human.list.medium.no"/></td>
                            </c:otherwise>
                        </c:choose>   
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                        <form action="${pageContext.request.contextPath}/human/removeHumanFromHouse" method="post">
                            <input type="hidden" name="houseId" value=${house.id}>
                            <input type="hidden" name="humanId" value=${human.id}>
                            <input id="generalButton" type="submit" value="<fmt:message key='admin.button.removeFromHouse'/>">
                        </form>
                        </td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
            </table>
            <sec:authorize access="hasRole('ROLE_ADMIN')">    
            <form method="get" action="${pageContext.request.contextPath}/human/addHumanList">
                <input type="hidden" name="houseId" value="${house.id}">
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.addHuman'/>">
            </form>  
            </sec:authorize>
        </div>
            
        <div id="ghosts">
            <h2><fmt:message key="house.ghosts"/></h2>
            <table class="listTable">
                <tr>
                    <th><fmt:message key="ghost.list.name"/></th>
                    <th><fmt:message key="ghost.list.energy"/></th>
                    <th><fmt:message key="ghost.list.peopleHaunted"/></th>
                </tr>
                <c:forEach items="${ghostsInHouse}" var="ghost">
                    <tr>
                        <td><a href="${pageContext.request.contextPath}/ghost/showGhost/${ghost.id}">${ghost.name}</a></td>
                        <td>${ghost.energyToHaunt}</td>
                        <td>${ghost.numberOfPeopleHaunted}</td>    
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                        <form action="${pageContext.request.contextPath}/ghost/removeGhostFromHouse" method="post">
                            <input type="hidden" name="houseId" value=${house.id}>
                            <input type="hidden" name="ghostId" value=${ghost.id}>
                            <input id="generalButton" type="submit" value="<fmt:message key='admin.button.removeFromHouse'/>">
                        </form>
                        </td>  
                        </sec:authorize>
                     </tr>
                </c:forEach>
            </table>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
            <form method="get" action="${pageContext.request.contextPath}/ghost/addGhostList">
                <input type="hidden" name="houseId" value="${house.id}">
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.addGhost'/>">
            </form>
            </sec:authorize>
        </div>
    </div>    
</jsp:attribute>
</my:layout>
