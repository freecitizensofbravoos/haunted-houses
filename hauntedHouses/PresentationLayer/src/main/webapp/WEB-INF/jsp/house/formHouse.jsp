<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table border="0">        
    <tr>
        <td><b><fmt:message key="house.list.name"/></b></td>
        <td><form:input path="name" required="true" htmlEscape="true"/></td>
        <td><form:errors path="name" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="house.list.hauntStarted"/></b></td>
        <td><form:input id="name" path="startOfHaunting" placeholder="dd/MM/yyyy" htmlEscape="true"/></td>
        <td><form:errors path="startOfHaunting" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="house.list.history"/></b></td>
        <td><form:textarea path="history" htmlEscape="true"/></td>
        <td><form:errors path="history" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="house.list.state"/></b></td>
        <td><form:input path="address.addrState" htmlEscape="true"/></td>
        <td><form:errors path="address.addrState" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="house.list.city"/></b></td>
        <td><form:input path="address.addrCity" htmlEscape="true"/></td>
        <td><form:errors path="address.addrCity" cssClass="error"/></td>
    </tr>
    <tr>
        <td><b><fmt:message key="house.list.street"/></b></td>
        <td><form:input path="address.addrStreet" htmlEscape="true"/></td>
        <td><form:errors path="address.addrStreet" cssClass="error"/></td>
    </tr>
</table>
