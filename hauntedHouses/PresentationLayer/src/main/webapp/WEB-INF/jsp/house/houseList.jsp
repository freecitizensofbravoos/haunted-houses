<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" session="false" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="index.title"/>
<my:layout title="${title}">
<jsp:attribute name="body">
    <%@include file="houseNavigationForm.jsp"%>
    <div id="framecontent">
        <div id="filter">
            <form method="get" action="${pageContext.request.contextPath}/house/houseList">
                <b><fmt:message key='house.filter.text'/></b>
                <input type="text" name="country" value="${filterValue}">
                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.find'/>">
            </form>
        </div>
        <table class="listTable" style="width:100%">
            <tr>
                <th><fmt:message key="house.list.name"/></th>
                <th><fmt:message key="house.list.street"/></th>
                <th><fmt:message key="house.list.city"/></th>
                <th><fmt:message key="house.list.state"/></th>
                <th><fmt:message key="house.list.hauntStarted"/></th>
             </tr>        
            <c:forEach items="${houses}" var="house">
                <tr>
                    <td><a href="${pageContext.request.contextPath}/house/showHouse/${house.id}">${house.name}</a></td>
                    <td>${house.address.addrStreet}</td>
                    <td>${house.address.addrCity}</td>
                    <td>${house.address.addrState}</td>
                    <td><fmt:formatDate type="date" value="${house.startOfHaunting}"/></td>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <td>
                            <form method="get" action="${pageContext.request.contextPath}/house/update/${house.id}">
                                <input id="generalButton" type="submit" value="<fmt:message key='admin.button.edit'/>">
                            </form>
                        </td>
                        <td>
                            <form action="${pageContext.request.contextPath}/house/houseList/delete" method="post">
                                <input type="hidden" name="houseId" value=${house.id}>
                                <input id="generalButton" type="submit" onclick="return confirm('<fmt:message key="dialog.confirm.delete"/>');" VALUE=<fmt:message key="admin.button.delete"/>>                           
                            </form>
                        </td>
                    </sec:authorize>
                </tr>
            </c:forEach>               
         </table>
    </div>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
        <div id="createButton">
            <form method="get" action="${pageContext.request.contextPath}/house/createHouse">
                <input id="createButton" type="submit" value="<fmt:message key='admin.button.create.house'/>">
            </form>
        </div>
    </sec:authorize>
</jsp:attribute>
</my:layout>
