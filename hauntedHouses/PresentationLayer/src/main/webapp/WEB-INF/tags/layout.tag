<!DOCTYPE html>
<%@ tag pageEncoding="utf-8" dynamic-attributes="dynattrs" trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" required="true" %>
<%@ attribute name="head" fragment="true" %>
<%@ attribute name="body" fragment="true" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="${pageContext.request.locale}">
    <head>
        <title><c:out value="${title}"/></title>
        <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css"/>
        <jsp:invoke fragment="head"/>
    </head>
    <body>
        <div id="header">
            <div class="title"><c:out value="${title}"/></div>            

            <div class="language">
                <th>
                <a href="?lang=en"><img src="${pageContext.request.contextPath}/images/english.png" alt="en" title="English"></a>
                <a href="?lang=sk"><img src="${pageContext.request.contextPath}/images/slovak.png" alt="sk" title="Slovensky"></a>
                <a href="?lang=cs"><img src="${pageContext.request.contextPath}/images/czech.png" alt="cz" title="Česky"></a>
                </th>
                <th> 
                <div id="userInfo">
                    <c:url value="/j_spring_security_logout" var="logoutUrl" />

                    <!-- csrt for log out-->
                    <form action="${logoutUrl}" method="post" id="logoutForm">
                      <input type="hidden" 
                            name="${_csrf.parameterName}"
                            value="${_csrf.token}" />
                    </form>

                    <script>
                            function formSubmit() {
                                    document.getElementById("logoutForm").submit();
                            }
                    </script>

                    <c:if test="${pageContext.request.userPrincipal.name != null}">
                            ${pageContext.request.userPrincipal.name} | <a
                                            href="javascript:formSubmit()"><fmt:message key="registrationMenu.logout"/></a>
                    </c:if>
                    <c:if test="${pageContext.request.userPrincipal.name == null}">
                        <a href="${pageContext.request.contextPath}/register"><fmt:message key="registrationMenu.signup"/></a> |
                        <a href="${pageContext.request.contextPath}/login"><fmt:message key="registrationMenu.login"/></a>
                    </c:if>
                </div>
                </th>
            </div>
        </div>   

        <div id="content">            
            <jsp:invoke fragment="body"/>
        </div>
    </body>
</html>
