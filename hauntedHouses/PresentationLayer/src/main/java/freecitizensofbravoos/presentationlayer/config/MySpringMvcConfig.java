package freecitizensofbravoos.presentationlayer.config;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 *
 * @author andris
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "freecitizensofbravoos")
@ImportResource({"classpath*:serviceContext.xml"})
public class MySpringMvcConfig extends WebMvcConfigurerAdapter {
    
    /**
     * Maps the main page to a specific view.
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
        registry.addViewController("/register").setViewName("registration");
        registry.addViewController("/register").setViewName("registrationSuccess");
        registry.addViewController("/houseList").setViewName("house/houseList");
        registry.addViewController("/ghostList").setViewName("ghost/ghostList");
        registry.addViewController("/humanList").setViewName("human/humanList");
        registry.addViewController("/abilityList").setViewName("ability/abilityList");
        registry.addViewController("/createAbility").setViewName("ability/createAbility");
        registry.addViewController("/createAbility").setViewName("ability/abilityList");
        registry.addViewController("/createGhost").setViewName("ghost/createGhost");
        registry.addViewController("/createGhost").setViewName("ghost/ghostList");
        registry.addViewController("/createHouse").setViewName("house/createHouse");
        registry.addViewController("/createHouse").setViewName("house/houseList");
        registry.addViewController("/createHuman").setViewName("human/createHuman");
        registry.addViewController("/createHuman").setViewName("human/humanList");
    }

    /**
     * Enables default Tomcat servlet that serves static files.
     * @param configurer
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    /**
     * Provides mapping from view names to JSP pages in WEB-INF/jsp directory.
     * @return 
     */
    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    /**
     * Provides localized messages.
     * @return 
     */
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("texts");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
    
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        localeChangeInterceptor.setParamName("lang");
        registry.addInterceptor(localeChangeInterceptor);
    }

    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver cookieLocaleResolver = new CookieLocaleResolver();
        cookieLocaleResolver.setDefaultLocale(StringUtils.parseLocaleString("en"));
        return cookieLocaleResolver;
    }   
    
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }    
}
