package freecitizensofbravoos.presentationlayer.web;

import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.service.GhostService;
import freecitizensofbravoos.servicelayer.service.HouseService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author kika
 */
@Controller
@RequestMapping(value = "/ghost")
public class GhostController {
    
    @Autowired
    private GhostService ghostService;
    @Autowired
    private HouseService houseService;

    public void setGhostService(GhostService ghostService) {
        this.ghostService = ghostService;
    }  
    
    @RequestMapping(value = "/ghostList", method = RequestMethod.GET)
    public String viewAllGhosts(Map<String, Object> model,
            @ModelAttribute("name") String name) {
        List<GhostDTO> ghosts;
        if (name.isEmpty()) {
            ghosts = ghostService.findAllGhosts();
        } else {
            ghosts = ghostService.findGhostsByName(name);
        }
        Collections.sort(ghosts, new Comparator<GhostDTO>(){
            @Override
            public int compare(GhostDTO o1, GhostDTO o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("ghostList", ghosts);        
        model.put("filterValue", name);
        return "ghost/ghostList";
    } 
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String viewUpdate(@PathVariable long id, Model model) {
        GhostDTO ghost = ghostService.findGhost(id);
        model.addAttribute("ghost", ghost);        
        return "ghost/editGhost";
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("ghost") GhostDTO ghost,
            BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "ghost/editGhost";
        }
        HouseDTO house = ghostService.findGhost(ghost.getId()).getHouse();
        ghost.setHouse(house);
        ghostService.updateGhost(ghost);
        return "redirect:/ghost/showGhost/" + ghost.getId().toString();
    }
    
    @RequestMapping(value = "/createGhost", method = RequestMethod.GET)
    public String viewCreateGhost(Map<String, Object> model,
            @ModelAttribute("houseId") Long houseId) {
        GhostDTO ghost = new GhostDTO();    
        model.put("createGhostForm", ghost);
        if(houseId >= 0)
        {
            model.put("houseId", houseId);
        }
        return "ghost/createGhost";
    }
    
    @RequestMapping(value = "/createGhost", method = RequestMethod.POST)
    public String processCreateGhost(@Valid @ModelAttribute("createGhostForm") GhostDTO ghostDTO,
            BindingResult bindingResult, @ModelAttribute("houseId") Long houseId,
            Map<String, Object> model) {
        if (bindingResult.hasErrors()) {
            return "ghost/createGhost";
        }
        if(houseId < 0)
        {
            ghostService.createGhost(ghostDTO);
            return "redirect:/ghost/ghostList";
        }
        HouseDTO houseDTO = houseService.findHouse(houseId);        
        houseDTO.addGhost(ghostDTO);
        houseService.updateHouse(houseDTO);
        
        model.put("ghostsInHouse", houseService.findGhostsOfHouse(houseDTO));
        model.put("humansInHouse", houseService.findHumansOfHouse(houseDTO));
        model.put("house", houseDTO);        
        
        return "redirect:/house/showHouse/" + houseId.toString();
    }
    
    @RequestMapping(value = "/ghostList/delete", method = RequestMethod.POST)
    public String processDeleteGhost(@ModelAttribute("ghostId") Long ghostId) {
        ghostService.deleteGhost(ghostId);  
        return "redirect:/ghost/ghostList";        
    }
    
    @RequestMapping(value="/showGhost/{ghostId}",method=RequestMethod.GET)
    public String processShowGhost(@PathVariable long ghostId,
             Map<String, Object> model){
        GhostDTO ghostDTO = ghostService.findGhost(ghostId);
        List<AbilityDTO> abilities = ghostService.findAbilitiesOfGhost(ghostId);
        Collections.sort(abilities, new Comparator<AbilityDTO>(){
            @Override
            public int compare(AbilityDTO o1, AbilityDTO o2)
            {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("ghost",ghostDTO);
        model.put("abilities", abilities);
        return "ghost/showGhost";
    }
    
    @RequestMapping(value = "/addGhostList", method = RequestMethod.GET)
    public String viewAddGhostList(Map<String, Object> model,
            @ModelAttribute("houseId") Long houseId) {
            
            List<GhostDTO> ghosts = ghostService.findAllGhosts();
            List<GhostDTO> homelessGhosts = new ArrayList<>();
            for(GhostDTO g: ghosts)
            {
                if(g.getHouse() == null)
                {
                    homelessGhosts.add(g);
                }
            }     
            Collections.sort(homelessGhosts, new Comparator<GhostDTO>(){
                @Override
                public int compare(GhostDTO o1, GhostDTO o2)
                {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
            model.put("homelessGhosts", homelessGhosts); 
            model.put("houseId", houseId);
            
        return "ghost/addGhostList";
    }
    
    @RequestMapping(value = "/addGhostList", method = RequestMethod.POST)
    public String processAddGhost(@ModelAttribute("houseId") Long houseId, @ModelAttribute("ghostId") Long ghostId) {
        HouseDTO house = houseService.findHouse(houseId);
        house.getGhosts().add(ghostService.findGhost(ghostId));
        houseService.updateHouse(house);        
        return "redirect:/house/showHouse/" + houseId.toString();
    }
    
    @RequestMapping(value = "/removeGhostFromHouse", method = RequestMethod.POST)
    public String processRemoveGhostFromHouse(@ModelAttribute("houseId") Long houseId, @ModelAttribute("ghostId") Long ghostId) {
        HouseDTO house = houseService.findHouse(houseId);
        GhostDTO ghost = ghostService.findGhost(ghostId);
        house.setGhosts(houseService.findGhostsOfHouse(house));        
        house.getGhosts().remove(ghost);
        ghost.setHouse(null);  
        ghostService.updateGhost(ghost);
        houseService.updateHouse(house);
        return "redirect:/house/showHouse/" + houseId.toString();
    }
}
