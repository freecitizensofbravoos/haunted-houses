package freecitizensofbravoos.presentationlayer.web.rest;

import freecitizensofbravoos.presentationlayer.web.rest.exceptions.BadRequestException;
import freecitizensofbravoos.presentationlayer.web.rest.exceptions.NotFoundException;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.service.GhostService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author xkaiser1
 */
@RestController
@RequestMapping("/rest/ghosts")
public class GhostRestController {
    
    @Autowired
    private GhostService ghostService;
    
    @RequestMapping(method=RequestMethod.GET, headers="Accept=application/json")
    public List<GhostDTO> getGhosts() {
        RestAuthenticator.authenticate();
        return ghostService.findAllGhosts();
    }   
    
    @RequestMapping(value="byName", method=RequestMethod.GET, headers="Accept=application/json")
    public List<GhostDTO> getGhostsByName(@RequestParam(value = "name") String name) {
        RestAuthenticator.authenticate();
        return ghostService.findGhostsByName(name);
    }
    
    @RequestMapping(value="{id}", method = RequestMethod.GET, headers="Accept=application/json")
    public GhostDTO getGhost(@PathVariable Long id) {
        RestAuthenticator.authenticate();
        GhostDTO ghostDTO = ghostService.findGhost(id);
        if (ghostDTO == null) {
            throw new NotFoundException();
        }
        return ghostDTO;
    }  
    
    @RequestMapping(value="abilities/{id}", method=RequestMethod.GET, headers="Accept=application/json")
    public List<AbilityDTO> getAbilitiesOfGhost(@PathVariable Long id) {
        RestAuthenticator.authenticate();
        List<AbilityDTO> abilities = ghostService.findAbilitiesOfGhost(id);
        if (abilities == null) {
            throw new NotFoundException();
        }
        return abilities;
    } 
    
    @RequestMapping(value="update", method = RequestMethod.POST)
    public void updateGhost(@RequestBody GhostDTO ghost) {
        RestAuthenticator.authenticate();
        if (ghost.getName() == null || (ghost.getName().trim().length() == 0)) {
            throw new BadRequestException();
        } 
        ghostService.updateGhost(ghost);
    }
     
    @RequestMapping(value="/delete/{id}" , method = RequestMethod.PUT)
    public void deleteGhost(@PathVariable Long id) {        
        RestAuthenticator.authenticate();
        GhostDTO ghostDTO = ghostService.findGhost(id);
        if (ghostDTO == null) {
            throw new NotFoundException();
        }
        ghostService.deleteGhost(id);
    }
    
    @RequestMapping(value="create", method = RequestMethod.POST)
    public ResponseEntity<Void> createGhost(@RequestBody GhostDTO ghost) {
        RestAuthenticator.authenticate();
        if (ghost.getName() == null || (ghost.getName().trim().length() == 0)) {
            throw new BadRequestException();
        } 
        ghostService.createGhost(ghost);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
