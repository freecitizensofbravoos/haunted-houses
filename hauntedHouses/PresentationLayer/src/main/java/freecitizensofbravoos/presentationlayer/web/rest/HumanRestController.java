package freecitizensofbravoos.presentationlayer.web.rest;

import freecitizensofbravoos.presentationlayer.web.rest.exceptions.BadRequestException;
import freecitizensofbravoos.presentationlayer.web.rest.exceptions.NotFoundException;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import freecitizensofbravoos.servicelayer.service.HumanService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author andris
 */
@RestController
@RequestMapping("/rest/humans")
public class HumanRestController {
    
    @Autowired
    private HumanService humanService;    
        
    @RequestMapping(method=RequestMethod.GET, headers="Accept=application/json")
    public List<HumanDTO> getHumans() {
        RestAuthenticator.authenticate();
        return humanService.findAllHumans();
    }
   
    @RequestMapping(value="mediums", method=RequestMethod.GET, headers="Accept=application/json")    
    public List<HumanDTO> getMediums() {
        RestAuthenticator.authenticate();
        return humanService.findAllMediums();
    }
    
    @RequestMapping(value="{id}", method = RequestMethod.GET, headers="Accept=application/json")
    public HumanDTO getHuman(@PathVariable Long id) {
        RestAuthenticator.authenticate();
        HumanDTO human = humanService.findHuman(id);
        if (human == null) {
            throw new NotFoundException();
        }
        return human;
    } 
     
    @RequestMapping(value="update", method = RequestMethod.POST)
    public void updateHuman(@RequestBody HumanDTO human) {        
        RestAuthenticator.authenticate();
        if (human.getName() == null || (human.getName().trim().length() == 0)) {
            throw new BadRequestException();
        } 
        humanService.updateHuman(human);
    }
     
    @RequestMapping(value="/delete/{id}" , method = RequestMethod.PUT)
    public void deleteHuman(@PathVariable Long id) {
        RestAuthenticator.authenticate();
        HumanDTO human = humanService.findHuman(id);
        if (human == null) {
            throw new NotFoundException();
        }
        humanService.deleteHuman(id);
    }  
    
    @RequestMapping(value="create", method = RequestMethod.POST)
    public ResponseEntity<Void> createHuman(@RequestBody HumanDTO human) {
        RestAuthenticator.authenticate();
        if (human.getName() == null || (human.getName().trim().length() == 0)) {
            throw new BadRequestException();
        }        
        humanService.createHuman(human);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }    
     
}
