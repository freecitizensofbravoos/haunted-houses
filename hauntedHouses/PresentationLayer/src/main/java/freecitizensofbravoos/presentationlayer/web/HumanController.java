package freecitizensofbravoos.presentationlayer.web;

import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import freecitizensofbravoos.servicelayer.service.HouseService;
import freecitizensofbravoos.servicelayer.service.HumanService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author andris
 */
@Controller
@RequestMapping("/human")
public class HumanController {
    
    @Autowired
    private HumanService humanService;
    @Autowired
    private HouseService houseService;

    public void setHumanService(HumanService humanService) {
        this.humanService = humanService;
    } 
    
    @RequestMapping(value = "/humanList", method = RequestMethod.GET)
    public String list(Map<String, Object> model,
            @ModelAttribute("type") String type) {  
        List<HumanDTO> humans;
        if (type.equals("medium")) {
            humans = humanService.findAllMediums();
        } else {
            humans = humanService.findAllHumans();
        }
        Collections.sort(humans, new Comparator<HumanDTO>(){
            @Override
            public int compare(HumanDTO o1, HumanDTO o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("humans", humans);               
        model.put("filterValue", type);
        return "human/humanList";
    }  
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String viewUpdate(@PathVariable long id, Model model) {
        HumanDTO human = humanService.findHuman(id);
        model.addAttribute("human", human);        
        return "human/editHuman";
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("human") HumanDTO humanDTO, 
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "human/editHuman";
        }
        HouseDTO house = humanService.findHuman(humanDTO.getId()).getHouse();
        humanDTO.setHouse(house);
        humanService.updateHuman(humanDTO);        
        return "redirect:/human/showHuman/" + humanDTO.getId().toString();
    }
    
    @RequestMapping(value = "/createHuman", method = RequestMethod.GET)
    public String viewCreateHuman(Map<String, Object> model,
            @ModelAttribute("houseId") Long houseId) {
        HumanDTO human = new HumanDTO();    
        model.put("createHumanForm", human);
        if(houseId >= 0)
        {
            model.put("houseId", houseId);
        }        
        return "human/createHuman";
    }
    
    @RequestMapping(value = "/createHuman", method = RequestMethod.POST)
    public String processCreateHuman(@Valid @ModelAttribute("createHumanForm") HumanDTO humanDTO,
            BindingResult bindingResult, @ModelAttribute("houseId") Long houseId,
            Map<String, Object> model) {
        if (bindingResult.hasErrors()) {
            return "human/createHuman";
        }
        if(houseId < 0)
        {
            humanService.createHuman(humanDTO);
            return "redirect:/human/humanList";
        }
        HouseDTO houseDTO = houseService.findHouse(houseId);        
        houseDTO.addHuman(humanDTO);
        houseService.updateHouse(houseDTO);
        
        model.put("humansInHouse", houseService.findHumansOfHouse(houseDTO));
        model.put("ghostsInHouse", houseService.findGhostsOfHouse(houseDTO));
        model.put("house", houseDTO);   
        
        return "redirect:/house/showHouse/" + houseId.toString();
    }
    
    @RequestMapping(value="/showHuman/{humanId}",method=RequestMethod.GET)
    public String processShowHuman(@PathVariable long humanId,
             Map<String, Object> model){
        HumanDTO humanDTO = humanService.findHuman(humanId);
        model.put("human",humanDTO);
        return "human/showHuman";
    }
    
    @RequestMapping(value = "/humanList/delete", method = RequestMethod.POST)
    public String processDeleteHuman(@ModelAttribute("humanId") Long id) {
        humanService.deleteHuman(id);
        return "redirect:/human/humanList";        
    }
    
    @RequestMapping(value = "/addHumanList", method = RequestMethod.GET)
    public String viewAddHumanList(Map<String, Object> model,
        @ModelAttribute("houseId") Long houseId) {

        List<HumanDTO> humans = humanService.findAllHumans();
        List<HumanDTO> homelessHumans = new ArrayList<>();
        for(HumanDTO h: humans)
        {
            if(h.getHouse() == null)
            {
                homelessHumans.add(h);
            }
        }    
        Collections.sort(homelessHumans, new Comparator<HumanDTO>(){
            @Override
            public int compare(HumanDTO o1, HumanDTO o2)
            {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });            
        model.put("homelessHumans", homelessHumans); 
        model.put("houseId", houseId);
            
        return "human/addHumanList";
    }
    
    @RequestMapping(value = "/addHumanList", method = RequestMethod.POST)
    public String processAddHuman(@ModelAttribute("houseId") Long houseId, @ModelAttribute("humanId") Long humanId) {
        HouseDTO house = houseService.findHouse(houseId);
        house.getHumans().add(humanService.findHuman(humanId));
        houseService.updateHouse(house);        
        return "redirect:/house/showHouse/" + houseId.toString();
    } 
    
    @RequestMapping(value = "/removeHumanFromHouse", method = RequestMethod.POST)
    public String processRemoveGhostFromHouse(@ModelAttribute("houseId") Long houseId, @ModelAttribute("humanId") Long humanId) {
        HouseDTO house = houseService.findHouse(houseId);
        HumanDTO human = humanService.findHuman(humanId);
        house.setHumans(houseService.findHumansOfHouse(house));        
        house.getHumans().remove(human);
        human.setHouse(null);  
        humanService.updateHuman(human);
        houseService.updateHouse(house);
        return "redirect:/house/showHouse/" + houseId.toString();
    }
}
