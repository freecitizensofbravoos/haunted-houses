package freecitizensofbravoos.presentationlayer.web;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.service.AbilityService;
import freecitizensofbravoos.servicelayer.service.GhostService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author andris
 */
@Controller
@RequestMapping("/ability")
public class AbilityController {
    
    @Autowired
    private AbilityService abilityService;
    @Autowired
    private GhostService ghostService;

    public void setAbilityService(AbilityService abilityService) {
        this.abilityService = abilityService;
    }    
    
    @ModelAttribute("severities")
    public Severity[] severities() {
        return Severity.values();
    } 
    
    @RequestMapping(value = "/abilityList", method = RequestMethod.GET)
    public String list(Map<String, Object> model, 
            @ModelAttribute("severity") String severity) { 
        List<AbilityDTO> abilities;
        if (severity.isEmpty() || severity.equals("all")) {
            abilities = abilityService.findAllAbilities();
        } else {
            abilities = abilityService.findAbilitiesBySeverity(Severity.valueOf(severity));
        }
        Collections.sort(abilities, new Comparator<AbilityDTO>(){
            @Override
            public int compare(AbilityDTO o1, AbilityDTO o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("abilities", abilities);               
        model.put("filterValue", severity);
        return "ability/abilityList";
    }  
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String viewUpdate(@PathVariable long id, Model model) {
        AbilityDTO ability = abilityService.findAbility(id);
        model.addAttribute("ability", ability);
        return "ability/editAbility";
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("ability") AbilityDTO ability,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "ability/editAbility";
        }
        abilityService.updateAbility(ability);
        return "redirect:/ability/showAbility/" + ability.getId().toString();        
    }
    
    @RequestMapping(value="/showAbility/{abilityId}",method=RequestMethod.GET)
    public String processShowAbility(@PathVariable Long abilityId,
             Map<String, Object> model){
        AbilityDTO abilityDTO = abilityService.findAbility(abilityId);
        model.put("ability",abilityDTO);        
        return "ability/showAbility";
    }
    
    @RequestMapping(value = "/createAbility", method = RequestMethod.GET)
    public String viewCreateAbility(Map<String, Object> model,
            @ModelAttribute("ghostId") Long ghostId) {
        AbilityDTO ability = new AbilityDTO();    
        model.put("createAbilityForm", ability);
        if(ghostId >= 0)
        {
            model.put("ghostId", ghostId);
        }
        return "ability/createAbility";
    }
    
    @RequestMapping(value = "/createAbility", method = RequestMethod.POST)
    public String processCreateAbility(@Valid @ModelAttribute("createAbilityForm") AbilityDTO abilityDTO,
            BindingResult bindingResult, @ModelAttribute("ghostId") Long ghostId,
            Map<String, Object> model) {
        if (bindingResult.hasErrors()) {
            return "ability/createAbility";
        }
        if(ghostId < 0)
        {
            abilityService.createAbility(abilityDTO);
            return "redirect:/ability/abilityList";
        }
        GhostDTO ghostDTO = ghostService.findGhost(ghostId);
        for(AbilityDTO ability : ghostService.findAbilitiesOfGhost(ghostId)) {
            ghostDTO.addAbility(ability);
        }
        ghostDTO.addAbility(abilityDTO);
        ghostService.updateGhost(ghostDTO);
        
        model.put("abilities", ghostService.findAbilitiesOfGhost(ghostId));
        model.put("ghost", ghostDTO); 
        
        return "redirect:/ghost/showGhost/" + ghostId.toString();
    }
    
    @RequestMapping(value = "/abilityList/delete", method = RequestMethod.POST)
    public String processDeleteAbility(@ModelAttribute("abilityId") Long id) {
        abilityService.deleteAbility(id);
        return "redirect:/ability/abilityList";
    }
    
    @RequestMapping(value = "/addAbilityList", method = RequestMethod.GET)
    public String viewAddAbilityList(Map<String, Object> model,
            @ModelAttribute("ghostId") Long ghostId) {
            
            List<AbilityDTO> abilities = abilityService.findAllAbilities();
            List<AbilityDTO> ghostAbilities = new ArrayList<>(ghostService.findGhost(ghostId).getAbilities());            
            abilities.removeAll(ghostAbilities);
            Collections.sort(ghostAbilities, new Comparator<AbilityDTO>(){
                @Override
                public int compare(AbilityDTO o1, AbilityDTO o2)
                {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
            Collections.sort(abilities, new Comparator<AbilityDTO>(){
                @Override
                public int compare(AbilityDTO o1, AbilityDTO o2)
                {
                    return o1.getName().compareToIgnoreCase(o2.getName());
                }
            });
            model.put("ghostAbilities", ghostAbilities);
            model.put("remainingAbilities", abilities);
        return "ability/addAbilityList";
    }
    
    @RequestMapping(value = "/addAbilityList", method = RequestMethod.POST)
    public String processAddAbility(@ModelAttribute("abilityId") Long abilityId, @ModelAttribute("ghostId") Long ghostId) {
        GhostDTO ghost = ghostService.findGhost(ghostId);
        ghost.getAbilities().add(abilityService.findAbility(abilityId));
        ghostService.updateGhost(ghost);        
        return "redirect:/ghost/showGhost/" + ghostId.toString();
    }  
    
    @RequestMapping(value = "/removeAbilityFromGhost", method = RequestMethod.POST)
    public String processRemoveAbilityFromGhost(@ModelAttribute("abilityId") Long abilityId, @ModelAttribute("ghostId") Long ghostId) {
        GhostDTO ghost = ghostService.findGhost(ghostId);
        ghost.getAbilities().remove(abilityService.findAbility(abilityId));
        ghostService.updateGhost(ghost);
        return "redirect:/ghost/showGhost/" + ghostId.toString();
    }
}
