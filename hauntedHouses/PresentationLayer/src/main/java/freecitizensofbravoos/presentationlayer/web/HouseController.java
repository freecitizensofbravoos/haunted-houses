package freecitizensofbravoos.presentationlayer.web;


import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import freecitizensofbravoos.servicelayer.service.HouseService;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author andris
 */
@Controller
@RequestMapping("/house")
public class HouseController {
    
    @Autowired
    private HouseService houseService;  

    public void setHouseService(HouseService houseService) {        
        this.houseService = houseService;
    } 
    
    @RequestMapping(value = "/houseList", method = RequestMethod.GET)
    public String viewAllHouses(Map<String, Object> model,
            @ModelAttribute("country") String country) {
        List<HouseDTO> houses;
        if (country.isEmpty()) {
            houses = houseService.findAllHouses();
        } else {
            houses = houseService.findHousesByCountry(country);
        }
        Collections.sort(houses, new Comparator<HouseDTO>(){
            @Override
            public int compare(HouseDTO o1, HouseDTO o2) {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("houses", houses);      
        model.put("filterValue", country);
        return "house/houseList";
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String viewUpdate(@PathVariable long id, Model model) {
        HouseDTO house = houseService.findHouse(id);
        model.addAttribute("house", house);
        return "house/editHouse";
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute("house") HouseDTO house,
            BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "house/editHouse";
        }
        houseService.updateHouse(house);
        return "redirect:/house/showHouse/" + house.getId().toString();
    }
    
    @RequestMapping(value = "/createHouse", method = RequestMethod.GET)
    public String viewCreateHouse(Map<String, Object> model) {
        Date date = new Date();
        model.put("date",date);
        HouseDTO house = new HouseDTO();    
        model.put("createHouseForm", house);
        return "house/createHouse";
    }
    
    @RequestMapping(value = "/createHouse", method = RequestMethod.POST)
    public String processCreateHouse(@Valid @ModelAttribute("createHouseForm") HouseDTO houseDTO,
            BindingResult bindingResult, Map<String, Object> model) {
        if (bindingResult.hasErrors()) {
            return "house/createHouse";
        }
        houseService.createHouse(houseDTO);
        return "redirect:/house/houseList";
    }
    
    @RequestMapping(value="/showHouse/{houseId}",method=RequestMethod.GET)
    public String processShowHouse(@PathVariable long houseId,
             Map<String, Object> model){
        HouseDTO houseDTO = houseService.findHouse(houseId);
        List<HumanDTO> humans = houseService.findHumansOfHouse(houseDTO);
        List<GhostDTO> ghosts = houseService.findGhostsOfHouse(houseDTO);
        Collections.sort(humans, new Comparator<HumanDTO>(){
            @Override
            public int compare(HumanDTO o1, HumanDTO o2)
            {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });        
        Collections.sort(ghosts, new Comparator<GhostDTO>(){
            @Override
            public int compare(GhostDTO o1, GhostDTO o2)
            {
                return o1.getName().compareToIgnoreCase(o2.getName());
            }
        });
        model.put("humansInHouse", humans);
        model.put("house",houseDTO);
        model.put("ghostsInHouse", ghosts);
        return "house/showHouse";
    }
    
    @RequestMapping(value = "/houseList/delete", method = RequestMethod.POST)
    public String processDeleteHouse(@ModelAttribute("houseId") Long id) {
        houseService.deleteHouse(id);
        return "redirect:/house/houseList";
    }    
}
