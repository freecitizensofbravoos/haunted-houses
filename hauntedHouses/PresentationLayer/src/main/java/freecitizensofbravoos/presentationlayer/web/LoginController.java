package freecitizensofbravoos.presentationlayer.web;

import freecitizensofbravoos.servicelayer.dto.UserDataDTO;
import freecitizensofbravoos.servicelayer.service.UserDataService;
import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;
import javax.validation.Valid;
import org.springframework.validation.BindingResult;

/**
 *
 * @author kika
 */
@Controller
public class LoginController{
 
    @Autowired
    private UserDataService userDataService;   

    public void setUserDataService(UserDataService userDataService) {
        this.userDataService = userDataService;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
        @RequestParam(value = "error", required = false) String error,
        @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

            return model;
    }
      
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String viewRegistration(Map<String, Object> model, 
            @RequestParam(value = "used", required = false) String used) {
        
        UserDataDTO userData = new UserDataDTO();   
        model.put("userForm", userData);
        if (used != null) {
            model.put("usedError", "Name has been already used.");
        }
        return "registration";
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String processRegistration(@Valid @ModelAttribute("userForm") UserDataDTO userDataDTO,
        BindingResult bindingResult, Map<String, Object> model) {           
        
        if(userDataService.findUserData(userDataDTO.getUsername()) != null)
        {
            model.put("used", "This name is used. Find another");
            return "redirect:/register";
        }       
        if (bindingResult.hasErrors())
        {
            return "/registration"; 
        }       
        
        userDataDTO.setAuthorities("ROLE_USER");
        userDataDTO.setEnabled(true);
        userDataService.createUserData(userDataDTO); 
        
        return "redirect:/login";
    }
     
}

