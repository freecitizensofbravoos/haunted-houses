package freecitizensofbravoos.presentationlayer.web.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author andris
 */
@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="400 Bad request")
public class BadRequestException extends RuntimeException {
    public BadRequestException() { super(); }
    public BadRequestException(String s) { super(s); }
    public BadRequestException(String s, Throwable throwable) { super(s, throwable); }
    public BadRequestException(Throwable throwable) { super(throwable); }
}
