package freecitizensofbravoos.presentationlayer.web.rest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author xkaiser1
 */
@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="404 Not Found")  // 404
public class NotFoundException extends RuntimeException{
    public NotFoundException() { super(); }
    public NotFoundException(String s) { super(s); }
    public NotFoundException(String s, Throwable throwable) { super(s, throwable); }
    public NotFoundException(Throwable throwable) { super(throwable); }
}
