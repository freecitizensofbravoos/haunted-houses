/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package freecitizensofbravoos.presentationlayer.web.rest;

import java.util.HashSet;
import java.util.Set;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author xurge
 */
public class RestAuthenticator {
    
    private static final String USER_NAME = "rest";
    private static final String PASSWORD = "rest";
    
    public static void authenticate() {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();        
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_NAME, 
                PASSWORD, grantedAuthorities);
        SecurityContextHolder.getContext().setAuthentication(token);
    } 
    
}
