package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.Address;
import freecitizensofbravoos.haunted_houses.dao.HouseManager;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @author xkaiser1
 */
public class HouseServiceImplTest {
        
    private HouseServiceImpl service;   
    private HouseManager manager;

    @Before
    public void setUp() {
        manager = mock(HouseManager.class);
        service = new HouseServiceImpl();
        service.setManager(manager);
    }
    
    @Test
    public void testCreateHouse() {
        
        System.out.println("createHouse");
        HouseDTO house = new HouseDTO();
        house.setName("House of Torment");
        house.setAddress(new Address("New Mexico", "Dark Shire", "Wall", "7"));
        house.setStartOfHaunting(new Date());
        house.setHistory("history");
        service.createHouse(house);
    }
    
    @Test
    public void testFindHouse() {
        System.out.println("findHouse");
        House house = new House();
        house.setName("Blackwood House");
        house.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house.setStartOfHaunting(new Date());
        house.setHistory("history");
        Long houseId = 0L;
        when(manager.getHouse(houseId)).thenReturn(house);

        HouseDTO result = service.findHouse(houseId);
        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        HouseDTO houseDTO = mapper.map(house, HouseDTO.class);
        assertEquals(houseDTO, result);
    }
    
    @Test
    public void testFindAllHouses() {
        System.out.println("findAllHouses");
        House house1 = new House();
        house1.setName("Blackwood House");
        house1.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house1.setStartOfHaunting(new Date());
        house1.setHistory("history");
        
        House house2 = new House();
        house2.setName("House of Torment");
        house2.setAddress(new Address("Mexico", "Dark Shire", "Wall", "7"));
        house2.setStartOfHaunting(new Date());
        house2.setHistory("history");
        
        
        assertTrue(service.findAllHouses().isEmpty());
        List<House> houseList = new ArrayList<>();
        List<HouseDTO> houseDTOList = new ArrayList<>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        houseList.add(house1);
        houseDTOList.add(mapper.map(house1, HouseDTO.class));
        houseList.add(house2);
        houseDTOList.add(mapper.map(house2, HouseDTO.class));
        
        when(manager.getAllHouses()).thenReturn(houseList);
        List<HouseDTO> resultList = service.findAllHouses();        
        assertArrayEquals("Obtained result list is not equal to the expected one.", houseDTOList.toArray(), resultList.toArray());

    }
    
    @Test
    public void testCreateHouseWithNullArgument() {
        System.out.println("createHouseWithNullArgument");
        boolean failed = false;
        try
        {
            service.createHouse(null);
        }
        catch(RuntimeException rex)
        {
            failed = true;
        }
        assertTrue("Null parameter didn't cause a runtime exception.", failed);  
    }
    
    @Test
    public void testUpdateHouse() throws ParseException {
        
        System.out.println("updateHouse");
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        
        HouseDTO house = new HouseDTO();    
        house.setName("Blackwood House");
        service.updateHouse(house);
        
        house.setName("Freaky Steak Bar");
        house.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house.setStartOfHaunting(sdf.parse("1100.06.03"));
        house.setHistory("history");
        service.updateHouse(house);
       
    }
    
    @Test
    public void testUpdateHouseWithNullArgument() {
        System.out.println("updateHouseWithNullArgument");
        boolean failed = false;
        try
        {
            service.updateHouse(null);
        }
        catch(RuntimeException rex)
        {
            failed = true;
        }
        assertTrue("Null parameter didn't cause a runtime exception.", failed);
        
    }

    @Test
    public void testDeleteHouse() {
        System.out.println("deleteHouse");   
        
        service.deleteHouse(Long.MIN_VALUE);
        service.deleteHouse(0L);
        service.deleteHouse(Long.MAX_VALUE);
    }

    @Test
    public void testDeleteHouseWithWrongAttributes() {
        System.out.println("deleteHouseWithWrongAttributes");
        
        boolean passed = true;
        try
        {
            service.deleteHouse(null);
        }
        catch(RuntimeException rex)
        {
            passed = false;
        }
        assertTrue("Null parameter did cause a runtime exception.", passed);
    }   
    
    @Test
    public void testFindHousesByCountry() throws ParseException{
        System.out.println("findAllHousesByCountry");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        House house1 = new House();
        house1.setName("Blackwood House");
        house1.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house1.setStartOfHaunting(sdf.parse("1100.06.03"));
        house1.setHistory("history");
        
        House house2 = new House();
        house2.setName("House of Torment");
        house2.setAddress(new Address("Mexico", "Dark Shire", "Wall", "7"));
        house2.setStartOfHaunting(sdf.parse("1700.06.03"));
        house2.setHistory("history");
        
        House house3 = new House();
        house3.setName("Dark House");
        house3.setAddress(new Address("Romania", "Green Wood", "Gray Street", "8"));
        house3.setStartOfHaunting(sdf.parse("1100.06.03"));
        house3.setHistory("history");
        
        assertTrue(service.findHousesByCountry("Mexico").isEmpty());
        
        List<House> houseList = new ArrayList<>();
        List<HouseDTO> expectedHouseDTOList = new ArrayList<>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        houseList.add(house1);
        expectedHouseDTOList.add(mapper.map(house1, HouseDTO.class));
        
        houseList.add(house2);
        
        houseList.add(house3);
        expectedHouseDTOList.add(mapper.map(house3, HouseDTO.class));
        
        when(manager.getAllHouses()).thenReturn(houseList);
        List<HouseDTO> resultList = service.findHousesByCountry("Romania");        
        assertArrayEquals("Obtained result list is not equal to the expected one. House 1 and 3 expected", expectedHouseDTOList.toArray(), resultList.toArray());
        
        assertTrue(service.findHousesByCountry("Brno").isEmpty());
    }
    
    @Test
    public void testFindGhostsOfHouse() throws ParseException{
        System.out.println("findGhostsOfHouse");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        House house1 = new House();
        house1.setName("Blackwood House");
        house1.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house1.setStartOfHaunting(sdf.parse("1100.06.03"));
        house1.setHistory("history");
        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        HouseDTO houseDTO1 = mapper.map(house1, HouseDTO.class);
        assertTrue(service.findGhostsOfHouse(houseDTO1).isEmpty());
        
        Ghost ghost1 = new Ghost();
        ghost1.setName("Bloody Mary");
        ghost1.setNumberOfPeopleHaunted(13);
        house1.addGhost(ghost1);
        
        houseDTO1 = mapper.map(house1, HouseDTO.class);
        List<Ghost> ghosts = new ArrayList<>();
        ghosts.add(ghost1);
        when(manager.getAllGhostsOfHouse(house1.getId())).thenReturn(ghosts);
        List<GhostDTO> resultList = service.findGhostsOfHouse(houseDTO1);
        List<GhostDTO> expectedResultList = new ArrayList<>();
        expectedResultList.add(mapper.map(ghost1,GhostDTO.class));
        assertArrayEquals("Obtained result list is not equal to the expected one.", expectedResultList.toArray(), resultList.toArray());
      
    }
    
    @Test
    public void testFindHumansOfHouse() throws ParseException{
        System.out.println("findHumansOfHouse");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        House house1 = new House();
        house1.setName("Blackwood House");
        house1.setAddress(new Address("Romania", "Black Wood", "Fog street", "9"));
        house1.setStartOfHaunting(sdf.parse("1100.06.03"));
        house1.setHistory("history");
        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        HouseDTO houseDTO1 = mapper.map(house1, HouseDTO.class);
        assertTrue(service.findHumansOfHouse(houseDTO1).isEmpty());
        
        Human human1 = new Human();
        human1.setName("Homer");
        human1.setMedium(true);
        human1.setAfraidOf("Marge, empty fridge, darkness");
        human1.setDateOfBirth(sdf.parse("1960.07.08"));
        houseDTO1 = mapper.map(house1, HouseDTO.class);
        List<Human> humans = new ArrayList<>();
        humans.add(human1);
        when(manager.getAllHumansOfHouse(house1.getId())).thenReturn(humans);
        List<HumanDTO> resultList = service.findHumansOfHouse(houseDTO1);
        List<HumanDTO> expectedResultList = new ArrayList<>();
        expectedResultList.add(mapper.map(human1,HumanDTO.class));
        assertArrayEquals("Obtained result list is not equal to the expected one.", expectedResultList.toArray(), resultList.toArray());        
    }
}
