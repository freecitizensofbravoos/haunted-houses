package freecitizensofbravoos.servicelayer.service.impl;
import static org.mockito.Mockito.*;
import freecitizensofbravoos.haunted_houses.dao.HumanManager;
import freecitizensofbravoos.haunted_houses.entity.Human;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author xkostoln
 */
public class HumanServiceImplTest {
    private HumanServiceImpl service;
    private HumanManager manager;
    
    public HumanServiceImplTest() { } 
    
    @Before
    public void setUp() {
        manager = mock(HumanManager.class);
        service = new HumanServiceImpl();
        service.setManager(manager);
    }
    @Test
    public void testCreateHuman() {
        HumanDTO h = new HumanDTO();
        h.setName("Aragorn");
        h.setMedium(true);
        h.setAfraidOf("Lifes of his fellows");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        try {
            h.setDateOfBirth(sdf.parse("1960.07.08"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        service.createHuman(h);
    }
    
    @Test
    public void testUpdateHuman() {
        HumanDTO h = new HumanDTO();
        h.setName("Gandalf-sivy");
        service.updateHuman(h);
        
        h.setName("Gandalf-biely");
        service.updateHuman(h);
    }
    
    @Test
    public void testUpdateWithWrongParameter() {
        boolean failed = false;
        try
        {
            service.updateHuman(null);
        }
        catch(RuntimeException rex)
        {
            failed = true;
        }
        assertTrue("OK", failed);
    }
    
    @Test
    public void testDeleteHuman() {
        service.deleteHuman(0L);
    }
    
    @Test
    public void testFindAllHumans() {List<HumanDTO> resultList = service.findAllHumans();        
        assertTrue("OK", resultList.isEmpty());
        
        List<Human> humanList = new ArrayList<>();
        List<HumanDTO> humanDTOList = new ArrayList<>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        
        Human h1 = new Human();        
        h1.setName("Faramir");
        h1.setAfraidOf("Disappointing his father");
        try {
            h1.setDateOfBirth(sdf.parse("1990.06.08"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        h1.setMedium(false);
        humanList.add(h1);
        humanDTOList.add(mapper.map(h1, HumanDTO.class));
        
        Human h2 = new Human();
        h2.setName("Boromir");
        h2.setAfraidOf("Disappointing his father");
        try {
            h2.setDateOfBirth(sdf.parse("1992.10.08"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        h2.setMedium(false);
        humanList.add(h2);
        humanDTOList.add(mapper.map(h2, HumanDTO.class));
        
        when(manager.getAllHumans()).thenReturn(humanList);
        resultList = service.findAllHumans();        
        assertArrayEquals(humanDTOList.toArray(), resultList.toArray());
    }   
    
    @Test
    public void testFindAllMediums() {       
        List<Human> humanList = new ArrayList<>();
        when(manager.getAllHumans()).thenReturn(humanList);        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        List<HumanDTO> mediumDTOList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        
        Human h1 = new Human();        
        h1.setName("Pipi");
        h1.setAfraidOf("Not having fun");
        try {
            h1.setDateOfBirth(sdf.parse("1991.06.08"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        h1.setMedium(true);
        humanList.add(h1);
        mediumDTOList.add(mapper.map(h1, HumanDTO.class));
        
        Human h2 = new Human();
        h2.setName("Tom");
        h2.setAfraidOf("Return home late");
        try {
            h2.setDateOfBirth(sdf.parse("1993.10.12"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        h2.setMedium(false);
        humanList.add(h2);
        
        List<HumanDTO> resultList = service.findAllMediums();
        assertArrayEquals(mediumDTOList.toArray(), resultList.toArray());
    }
    
    @Test
    public void testFindHuman() {
        Human h = new Human();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        h.setName("Frodo");
        h.setAfraidOf("Nazgul");
        try {
            h.setDateOfBirth(sdf.parse("1992.10.08"));
        } catch (ParseException ex) {
            Logger.getLogger(HumanServiceImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        h.setMedium(false);
        Long id = 0L;
        when(manager.getHuman(id)).thenReturn(h);
        
        HumanDTO human = service.findHuman(id);
        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        HumanDTO humanDTO = mapper.map(human, HumanDTO.class);
        assertEquals(human, humanDTO);
    }
}
