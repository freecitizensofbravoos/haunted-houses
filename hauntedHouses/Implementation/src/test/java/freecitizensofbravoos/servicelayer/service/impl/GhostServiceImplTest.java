package freecitizensofbravoos.servicelayer.service.impl;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import freecitizensofbravoos.haunted_houses.dao.GhostManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author andris
 */
public class GhostServiceImplTest {
    
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    private GhostServiceImpl service;
    private GhostManager manager;
    
    @Before
    public void setUp() {
        manager = mock(GhostManager.class);
        service = new GhostServiceImpl();
        service.setManager(manager);
    }
    
    @Test
    public void testFindAllGhosts() throws ParseException {
        when(manager.getAllGhosts()).thenReturn(new ArrayList<Ghost>());
        List<GhostDTO> resultList = service.findAllGhosts();        
        assertTrue(resultList.isEmpty());
        
        List<Ghost> ghostList = new ArrayList<>();
        List<GhostDTO> ghostDTOList = new ArrayList<>();
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
        
        Ghost ghost1 = new Ghost();
        ghost1.setName("Crazy rabbit");
        ghost1.setNumberOfPeopleHaunted(137564); 
        ghost1.setBeginningOfHaunt(dateFormat.parse("12:00"));        
        ghost1.setEndOfHaunt(dateFormat.parse("03:00")); 
        ghostList.add(ghost1);
        ghostDTOList.add(mapper.map(ghost1, GhostDTO.class));
        
        Ghost ghost2 = new Ghost();
        ghost2.setName("Mr Clown");
        ghost2.setNumberOfPeopleHaunted(9);       
        ghost2.setBeginningOfHaunt(dateFormat.parse("06:00"));        
        ghost2.setEndOfHaunt(dateFormat.parse("20:00")); 
        ghostList.add(ghost2);
        ghostDTOList.add(mapper.map(ghost2, GhostDTO.class));
        
        when(manager.getAllGhosts()).thenReturn(ghostList);
        resultList = service.findAllGhosts();        
        assertArrayEquals(ghostDTOList.toArray(), resultList.toArray());
    }   
    
    @Test
    public void testFindGhostsByName() throws ParseException { 
        List<Ghost> ghostList = new ArrayList<>();
        List<GhostDTO> ghostDTOList = new ArrayList<>();
        when(manager.getAllGhosts()).thenReturn(ghostList);
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");
        
        Ghost ghost1 = new Ghost();
        ghost1.setName("Crazy rabbit");
        ghost1.setNumberOfPeopleHaunted(137564); 
        ghost1.setBeginningOfHaunt(dateFormat.parse("12:00"));        
        ghost1.setEndOfHaunt(dateFormat.parse("03:00")); 
        ghostList.add(ghost1);       
        
        List<GhostDTO> resultList = service.findGhostsByName("Mr Clown");        
        assertTrue(resultList.isEmpty());
        
        Ghost ghost2 = new Ghost();
        ghost2.setName("Mr Clown");
        ghost2.setNumberOfPeopleHaunted(9);       
        ghost2.setBeginningOfHaunt(dateFormat.parse("06:00"));        
        ghost2.setEndOfHaunt(dateFormat.parse("20:00")); 
        ghostList.add(ghost2);
        ghostDTOList.add(mapper.map(ghost2, GhostDTO.class));
        
        resultList = service.findGhostsByName("Mr Clown");        
        assertArrayEquals(ghostDTOList.toArray(), resultList.toArray());
    }
    
    @Test
    public void findAbilitiesOfGhost() throws ParseException { 
        List<AbilityDTO> abilityDTOList = new ArrayList<>();        
        
        Ghost ghost = new Ghost();
        ghost.setId(1L);
        ghost.setName("Mutant Pony");
        ghost.setNumberOfPeopleHaunted(15); 
        
        Ability a1 = new Ability();        
        a1.setName("Rainbow Fart");
        a1.setDescription("When you smell it you'll be out for at least a month.");
        ghost.addAbility(a1);
        abilityDTOList.add(mapper.map(a1, AbilityDTO.class));
        
        Ability a2 = new Ability();
        a2.setName("Pony Hurricane");
        a2.setDescription("It will take you far away to Pony  Land.");
        ghost.addAbility(a2);
        abilityDTOList.add(mapper.map(a2, AbilityDTO.class)); 
        
        when(manager.getGhost(ghost.getId())).thenReturn(ghost);
        List<AbilityDTO> resultList = service.findAbilitiesOfGhost(ghost.getId());        
        assertArrayEquals(abilityDTOList.toArray(), resultList.toArray());
    }
    
     @Test
    public void testFindGhost() throws ParseException {  
        Ghost ghost = new Ghost();
        ghost.setId(0L);
        ghost.setName("Casper");
        ghost.setNumberOfPeopleHaunted(0);
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm");        
        ghost.setBeginningOfHaunt(dateFormat.parse("23:59"));        
        ghost.setEndOfHaunt(dateFormat.parse("00:00"));
        
        when(manager.getGhost(ghost.getId())).thenReturn(ghost); 
        
        GhostDTO expectedGhost = mapper.map(ghost, GhostDTO.class);
        GhostDTO actualGhost = service.findGhost(0L);
        assertEquals(expectedGhost, actualGhost);
    } 
    
    @Test
    public void testCreateGhost() throws ParseException {
        try {
            service.createGhost(null);
        } catch (IllegalArgumentException ex) {
            // ok
        }
        
        GhostDTO ghost = new GhostDTO();
        ghost.setName("Frank Einstein");        
        service.createGhost(ghost);      
        
        Ghost g = mapper.map(ghost, Ghost.class);
        verify(manager).createGhost(g);
    }
    
    @Test
    public void testUpdateGhost() {
        try {
            service.updateGhost(null);
        } catch (IllegalArgumentException ex) {
            // ok
        }
        
        GhostDTO ghost = new GhostDTO();
        ghost.setName("Gengar");
        service.updateGhost(ghost);
        
        Ghost g = mapper.map(ghost, Ghost.class);
        verify(manager).updateGhost(g);
    }
    
    @Test
    public void testDeleteGhost() {
        service.deleteGhost(1L);        
        verify(manager).deleteGhost(1L);
    } 
}
