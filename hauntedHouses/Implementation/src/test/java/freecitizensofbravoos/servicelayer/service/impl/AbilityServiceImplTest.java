package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.haunted_houses.dao.AbilityManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 *
 * @author xmoravc6
 */
public class AbilityServiceImplTest {  
    private AbilityServiceImpl service;
    private AbilityManager manager;
    
    public AbilityServiceImplTest() { } 
    
    @Before
    public void setUp() {
        manager = mock(AbilityManager.class);
        service = new AbilityServiceImpl();
        service.setManager(manager);
    }

    /**
     * Test of findAllAbilities method, of class AbilityServiceImpl.
     */
    @Test
    public void testFindAllAbilities() {
        System.out.println("findAllAbilities");
        
        List<AbilityDTO> resultList = service.findAllAbilities();        
        assertTrue("Result list should be empty!", resultList.isEmpty());
        
        List<Ability> abilityList = new ArrayList<>();
        List<AbilityDTO> abilityDTOList = new ArrayList<>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        Ability a1 = new Ability();        
        a1.setName("Chills");
        a1.setDescription("Gives chills to any living being in immediate vicinity.");
        a1.setSeverity(Severity.Negligible);
        abilityList.add(a1);
        abilityDTOList.add(mapper.map(a1, AbilityDTO.class));
        
        Ability a2 = new Ability();
        a2.setName("Awaken the Undead");
        a2.setDescription("Can make even dead bones rise and walk the Earth again.");
        a2.setSeverity(Severity.Cataclysmic);
        abilityList.add(a2);
        abilityDTOList.add(mapper.map(a2, AbilityDTO.class));
        
        Ability a3 = new Ability();
        a3.setName("Unlock the Gates of Pandemonium");
        a3.setDescription("To invoke the hive-mind representing chaos.\n" +
            "Invoking the feeling of chaos.\n" +
            "With out order.\n" +
            "The Nezperdian hive-mind of chaos. Zalgo.\n" +
            "He who Waits Behind The Wall.\n" +
            "ZALGO!");
        a3.setSeverity(Severity.Apocalyptic);
        abilityList.add(a3);
        abilityDTOList.add(mapper.map(a3, AbilityDTO.class));
        
        when(manager.getAbilities()).thenReturn(abilityList);
        resultList = service.findAllAbilities();        
        assertArrayEquals("Obtained result list is not equal to the expected one.", abilityDTOList.toArray(), resultList.toArray());
    }

    /**
     * Test of findAbilitiesBySeverity method, of class AbilityServiceImpl.
     */
    @Test
    public void testFindAbilitiesBySeverity() {
        System.out.println("findAbilitiesBySeverity");
        
        List<AbilityDTO> resultList = service.findAbilitiesBySeverity(null);
        assertTrue("Result list should be empty!", resultList.isEmpty());
        
        resultList = service.findAbilitiesBySeverity(Severity.Annoying);        
        assertTrue("Result list should be empty!", resultList.isEmpty());
        
        List<Ability> abilityList = new ArrayList<>();
        when(manager.getAbilities()).thenReturn(abilityList);        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        List<AbilityDTO> nullSeverityList = new ArrayList<>();
        Ability a1 = new Ability();        
        a1.setName("Set severity to null.");
        a1.setDescription("Can set severity of this ability to null. Boo.");
        a1.setSeverity(null);
        abilityList.add(a1);
        nullSeverityList.add(mapper.map(a1, AbilityDTO.class));  
        
        resultList = service.findAbilitiesBySeverity(Severity.Annoying);        
        assertTrue("Result list should be empty!", resultList.isEmpty());
        
        List<AbilityDTO> annoyingAbilityDTOList = new ArrayList<>(); 
        
        Ability a2 = new Ability();
        a2.setName("Cold Breeze");
        a2.setDescription("Notably annoying while you are having a shower.");
        a2.setSeverity(Severity.Annoying);
        abilityList.add(a2);
        annoyingAbilityDTOList.add(mapper.map(a2, AbilityDTO.class));
        
        Ability a3 = new Ability();
        a3.setName("Voices inside Head");
        a3.setDescription("Singing the same song again and again.");
        a3.setSeverity(Severity.Annoying);
        abilityList.add(a3);
        annoyingAbilityDTOList.add(mapper.map(a3, AbilityDTO.class));
        
        Ability a4 = new Ability();
        a4.setName("Cause Nightmares");
        a4.setSeverity(Severity.Distressing);
        abilityList.add(a4);
        
        resultList = service.findAbilitiesBySeverity(Severity.Annoying);        
        assertArrayEquals("Obtained result list is not equal to the expected one.", annoyingAbilityDTOList.toArray(), resultList.toArray()); 
        
        resultList = service.findAbilitiesBySeverity(null);        
        assertArrayEquals("Obtained result list is not equal to the expected one.", nullSeverityList.toArray(), resultList.toArray());
    }

    /**
     * Test of createAbility method, of class AbilityServiceImpl.
     */
    @Test
    public void testCreateAbility() {
        System.out.println("createAbility");
        
        boolean failed = false;
        try
        {
            service.createAbility(null);
        }
        catch(IllegalArgumentException iaex)
        {
            failed = true;
        }
        assertTrue("IllegalArgumentException expected.", failed);
        
        AbilityDTO a = new AbilityDTO();
        a.setName("Morbid humor");
        a.setDescription("Dead people are cool.");
        a.setSeverity(Severity.Negligible);
        service.createAbility(a);
    }

    /**
     * Test of updateAbility method, of class AbilityServiceImpl.
     */
    @Test    
    public void testUpdateAbility() {
        System.out.println("updateAbility");
        
        boolean failed = false;
        try
        {
            service.updateAbility(null);
        }
        catch(IllegalArgumentException iaex)
        {
            failed = true;
        }
        assertTrue("IllegalArgumentException expected.", failed);
        
        AbilityDTO a = new AbilityDTO();    
        a.setName("Photobombing");
        service.updateAbility(a);
        
        a.setName("Photobombing");
        a.setDescription("Entity is purposely putting itself into the view of photographs. Highly contagious.");
        a.setSeverity(Severity.Annoying);
        service.updateAbility(a);
    }

    /**
     * Test of deleteAbility method, of class AbilityServiceImpl.
     */
    @Test
    public void testDeleteAbility() {
        System.out.println("deleteAbility");
        
        service.deleteAbility(Long.MIN_VALUE);
        service.deleteAbility(0L);
        service.deleteAbility(Long.MAX_VALUE);
    }
    
    /**
     * Test of findAbility method, of class AbilityServiceImpl.
     */
    @Test
    public void testFindAbility() {
        System.out.println("findAbilities");        
       
        AbilityDTO ability = service.findAbility(null); 
        assertNull("Result should be null!", ability);        
        
        AbilityDTO result = service.findAbility(0L);
        assertNull("Result should be null!", result);
        
        Ability a = new Ability();        
        a.setName("Cause bleeding from eyes");
        a.setDescription("Pretty much self-explanatory.");
        a.setSeverity(Severity.Harmful);        
        when(manager.getAbility(0L)).thenReturn(a);        
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        assertEquals("Expected and retrieved abilityDTO are not equal!", mapper.map(a, AbilityDTO.class), service.findAbility(0L));
    }    
}
