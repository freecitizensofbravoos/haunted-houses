package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.haunted_houses.dao.GhostManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.PersistenceContext;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

/**
 *
 * @author xmoravc6
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/testContext.xml")
public class GhostManagerImplTest {  
    
    @Autowired
    private GhostManager instance;
        
    @PersistenceContext
    private EntityManager em;
    
    @Autowired
    private PlatformTransactionManager transactionManager;
    private final TransactionDefinition def = new DefaultTransactionDefinition();
    
    private static FetchType fetch;
        
    @BeforeClass
    public static void setUpClass() 
    {
        fetch = null;
        try {
            ManyToMany m2m = Ghost.class.getDeclaredField("abilities").getAnnotation(ManyToMany.class);
            fetch = m2m.fetch();
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(GhostManagerImplTest.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
    @AfterClass
    public static void tearDownClass() {}
    
    @Before
    public void setUp() {}
    
    @After
    public void tearDown()
    {
        for (Ghost g : instance.getAllGhosts()) 
        {
            instance.deleteGhost(g.getId());
        }
    }
    
    public boolean ghostEquals(Ghost g1, Ghost g2)
    {
        return ghostEquals(g1, g2, false);
    }
    
    public boolean ghostEquals(Ghost g1, Ghost g2, boolean ignoreAbilities)
    {
        if (!Objects.equals(g1, g2))
        {
            return false;
        }
        if (!Objects.equals(g1.getId(), g2.getId()))
        {
            return false;
        }
        if (!Objects.equals(g1.getName(), g2.getName()))
        {
            return false;
        }  
        if (!Objects.equals(g1.getBeginningOfHaunt(), g2.getBeginningOfHaunt()))
        {
            return false;
        }
        if (!Objects.equals(g1.getEndOfHaunt(), g2.getEndOfHaunt()))
        {
            return false;
        }
        if (g1.getEnergyToHaunt() != g2.getEnergyToHaunt())
        {
            return false;
        }
        if (g1.getNumberOfPeopleHaunted() != g2.getNumberOfPeopleHaunted())
        {
            return false;
        }
        if (!ignoreAbilities && fetch == FetchType.EAGER)
        {
            if (!g1.getAbilities().equals(g2.getAbilities()))
            {
                return false;
            }
        }
        return Objects.equals(g1.getHouse(), g2.getHouse());
    }
    
    /**
     * Test of createGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testCreateGhost()
    {
        System.out.println("createGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("Bloody Mary");
        ghost.setNumberOfPeopleHaunted(13);
        instance.createGhost(ghost); 
        assertTrue("Failed to create the first ghost!", ghostEquals(ghost, em.find(Ghost.class, ghost.getId())));
        
        ghost = new Ghost();        
        ghost.setName("The Phantom of the Opera");
        Ability a = new Ability();
        a.setName("MVP");
        a.setDescription("MVP stands for 'Most Valuable Phantom'.");
        a.setSeverity(Severity.Negligible);
        ghost.addAbility(a);
        instance.createGhost(ghost);    
        Ghost retrieved = em.find(Ghost.class, ghost.getId());
        assertTrue("Failed to create the second ghost!", ghostEquals(ghost, retrieved));
        assertArrayEquals("Failed to persist abilities of the second ghost!", ghost.getAbilities().toArray(), retrieved.getAbilities().toArray());
    }

    /**
     * Test of getGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testGetGhost()
    {
        System.out.println("getGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("White Lady");
        ghost.setNumberOfPeopleHaunted(666);
        Ability a;
        if(fetch == FetchType.EAGER)
        {        
            a = new Ability();
            a.setName("Advanced Whitening");
            a.setDescription("Note: Haunted people are mostly dentists.");
            a.setSeverity(Severity.Distressing);
            ghost.addAbility(a);      
        }
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        Ghost retrieved = instance.getGhost(ghost.getId());
        assertTrue("Failed to retrieve the first ghost!", ghostEquals(ghost, retrieved));
        if(fetch == FetchType.EAGER)
        {
            assertArrayEquals("Failed to retrieve abilities of the first ghost!", ghost.getAbilities().toArray(), retrieved.getAbilities().toArray());
        }        
        
        ghost = new Ghost();    
        ghost.setName("Slimer");
        ghost.setNumberOfPeopleHaunted(4);
        if(fetch == FetchType.EAGER)
        {
            a = new Ability();
            a.setName("Slime");
            a.setDescription("Covers stuff with stinky greenish goo. It's good for skin, though.");
            a.setSeverity(Severity.Annoying);            
            ghost.addAbility(a);  
            
            a = new Ability();
            a.setName("Stench resistance");
            a.setSeverity(Severity.Negligible);
            ghost.addAbility(a); 
        }
        status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        retrieved = instance.getGhost(ghost.getId());
        assertTrue("Failed to retrieve the second ghost!", ghostEquals(ghost, retrieved));
        if(fetch == FetchType.EAGER)
        {
            assertArrayEquals("Failed to retrieve abilities of the second ghost!", ghost.getAbilities().toArray(), retrieved.getAbilities().toArray());
        }
    }

    /**
     * Test of updateGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testUpdateGhost()
    {
        System.out.println("updateGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("Schrödinger's cat");
        ghost.setNumberOfPeopleHaunted(1);
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        ghost.setNumberOfPeopleHaunted(ghost.getNumberOfPeopleHaunted() + 1);
        instance.updateGhost(ghost);
        assertTrue("Failed to update ghost!", ghostEquals(ghost, em.find(Ghost.class, ghost.getId())));
        
        Ability a = new Ability();
        a.setName("Superposition");
        a.setDescription("It's dead and alive at the same time!");
        a.setSeverity(Severity.Distressing);        
        
        ghost.addAbility(a); 
        instance.updateGhost(ghost);    
        
        Ghost retrieved = em.find(Ghost.class, ghost.getId());
        assertTrue("Failed to perform the second update of ghost!", ghostEquals(ghost, retrieved, true));
        assertTrue("Failed to persist ability of ghost!", ghost.getAbilities().toArray().length == retrieved.getAbilities().toArray().length);
        ((Ability) (ghost.getAbilities().toArray()[0])).setId(((Ability) (retrieved.getAbilities().toArray()[0])).getId());
        assertArrayEquals("Failed to update abilities of ghost!", ghost.getAbilities().toArray(), retrieved.getAbilities().toArray());
        
        ghost.removeAbility(a);
        instance.updateGhost(ghost);
        retrieved = em.find(Ghost.class, ghost.getId());
        assertTrue("Failed to perform the third update of ghost!", ghostEquals(ghost, retrieved));
        assertArrayEquals("Failed to remove abilities of ghost!", ghost.getAbilities().toArray(), retrieved.getAbilities().toArray());
    }
                                  
    /**
     * Test of deleteGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testDeleteGhost()
    {
        System.out.println("deleteGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("GGG (Good Ghost Greg)");
        ghost.setNumberOfPeopleHaunted(0); 
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        instance.deleteGhost(ghost.getId()); 
        assertNull("Failed to remove the first ghost!", em.find(Ghost.class, ghost.getId())); 
        
        ghost = new Ghost();        
        ghost.setName("Maxwell's demon");
        ghost.setNumberOfPeopleHaunted(1);
        ghost.setEnergyToHaunt(1);
        Ability a = new Ability();
        a.setName("Confuse physicists");
        a.setSeverity(Severity.Distressing);
        ghost.addAbility(a); 
        
        Ghost otherGhost = new Ghost();        
        otherGhost.setName("Schrödinger's cat");        
        otherGhost.addAbility(a);
        
        status = transactionManager.getTransaction(def);
        em.persist(ghost); 
        em.persist(otherGhost);  
        transactionManager.commit(status);
        instance.deleteGhost(ghost.getId());        
        assertNull("Failed to remove the second ghost!", em.find(Ghost.class, ghost.getId()));
        assertNotNull("Ability the second ghost was removed as well!", em.find(Ability.class, a.getId()));
    }

    /**
     * Test of getAllGhosts method, of class GhostManagerImpl. 
     */
    @Test
    public void testGetAllGhosts() {
        System.out.println("getAllGhosts");
        
        Ghost ghost = new Ghost();        
        ghost.setName("King Hamlet");
        ghost.setNumberOfPeopleHaunted(1); 
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        
        ghost = new Ghost();        
        ghost.setName("Beetlejuice");
        ghost.setNumberOfPeopleHaunted(5);
        status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        
        List<Ghost> ghosts = em.createQuery("SELECT g FROM Ghost g", Ghost.class).getResultList();
        assertArrayEquals("Failed to get all ghosts!", ghosts.toArray(), instance.getAllGhosts().toArray());
    }

    /**
     * Test of getAllAbilitiesOfGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testGetAllAbilitiesOfGhost() {
        System.out.println("getAllAbilitiesOfGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("Obi-Wan Kenobi");        
        Ability a = new Ability();
        a.setName("The Force");
        a.setSeverity(Severity.Fatal);
        ghost.addAbility(a);   
        Ability b = new Ability();
        b.setName("Pancake Cooking");
        b.setSeverity(Severity.Cataclysmic);
        ghost.addAbility(b);
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(ghost);  
        transactionManager.commit(status);
        assertArrayEquals("Failed to get abilities of ghost!", ghost.getAbilities().toArray(), instance.getAllAbilitiesOfGhost(ghost.getId()).toArray());
    }

    /**
     * Test of getHouseOfGhost method, of class GhostManagerImpl.
     */
    @Test
    public void testGetHouseOfGhost() {
        System.out.println("getHouseOfGhost");
        
        Ghost ghost = new Ghost();        
        ghost.setName("Velkej černej pes a mrtvej kostelník.");        
        
        House house = new House();
        house.setName("Mlejn");
        house.addGhost(ghost);    
        TransactionStatus status = transactionManager.getTransaction(def);
        em.persist(house);  
        transactionManager.commit(status);        
        assertEquals("Failed to get house of ghost!", house, instance.getGhost(ghost.getId()).getHouse());
    }
}
