package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.Address;
import freecitizensofbravoos.haunted_houses.dao.HouseManager;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author xurge
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/testContext.xml")
public class HouseManagerImplTest {
    
    @Autowired
    private HouseManager manager;

    @Before
    public void setUp() {
        for (House h : manager.getAllHouses()) {
            manager.deleteHouse(h.getId());
        }
    }
    
    @After
    public void tearDown() { 
    }
    
    @Test
    public void testCreateHouse() {
        House house = newHouse("House of Torment", new Address("New Mexico", "Dark Shire", "Wall", "7"), 
                new Date(), "history" );
        manager.createHouse(house);

        Long houseId = house.getId();
        assertNotNull(houseId);
        House result = manager.getHouse(houseId);
        assertEquals(house, result);
        assertDeepEquals(house, result);
    }
    
    @Test
    public void testGetHouse() {
        House house = newHouse("Blackwood House", new Address("Romania", "Black Wood", "Fog street", "9"), 
                new Date(), "history" );
        manager.createHouse(house);
        Long houseId = house.getId();

        House result = manager.getHouse(houseId);
        assertEquals(house, result);
        assertDeepEquals(house, result);
    }
    
    @Test
    public void testGetAllHouses() {
        House house1 = newHouse("Blackwood House", new Address("Romania", "Black Wood", "Fog street", "9"), 
                new Date(), "history" );
        House house2 = newHouse("House of Torment", new Address("Mexico", "Dark Shire", "Wall", "7"), 
                new Date(), "history" );
        
        assertTrue(manager.getAllHouses().isEmpty());

        manager.createHouse(house1);
        manager.createHouse(house2);

        List<House> expected = Arrays.asList(house1, house2);
        List<House> actual = manager.getAllHouses();        

        assertEquals(expected, actual);
        assertDeepEqualsHouse(expected, actual);
    }
    
    @Test
    public void testCreateHouseWithNullArgument() {

        try {
            manager.createHouse(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }   
    }
    
    @Test
    public void testUpdateHouse() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        House house = newHouse("House of Torment", new Address("Mexico", "Dark Shire", "Wall", "7"), 
                sdf.parse("1100.06.03"), "history" );
        manager.createHouse(house);
        Long houseId = house.getId();

        // Set name
        house = manager.getHouse(houseId);
        house.setName("Freaky Steak Bar");
        manager.updateHouse(house);
        house = manager.getHouse(houseId);
        assertEquals("Freaky Steak Bar", house.getName());
        assertEquals("Dark Shire", house.getAddress().getAddrCity());
        assertEquals(sdf.parse("1100.06.03"), house.getStartOfHaunting());
        assertEquals("history", house.getHistory());
        
        // Set address
        house = manager.getHouse(houseId);
        house.setAddress(new Address("USA", "Texas", "Keral st.", "15"));
        manager.updateHouse(house);
        house = manager.getHouse(houseId);
        assertEquals("Freaky Steak Bar", house.getName());
        assertEquals("Texas", house.getAddress().getAddrCity());
        assertEquals(sdf.parse("1100.06.03"), house.getStartOfHaunting());
        assertEquals("history", house.getHistory());
        
        // Set date
        house = manager.getHouse(houseId);
        house.setStartOfHaunting(sdf.parse("964.07.23"));
        manager.updateHouse(house);
        house = manager.getHouse(houseId);
        assertEquals("Freaky Steak Bar", house.getName());
        assertEquals("Texas", house.getAddress().getAddrCity());
        assertEquals(sdf.parse("964.07.23"), house.getStartOfHaunting());
        assertEquals("history", house.getHistory());

        // Set name to null
        house = manager.getHouse(houseId);
        house.setName(null);
        manager.updateHouse(house);
        house = manager.getHouse(houseId);
        assertNull(house.getName());
        assertEquals("Texas", house.getAddress().getAddrCity());
        assertEquals(sdf.parse("964.07.23"), house.getStartOfHaunting());
        assertEquals("history", house.getHistory());
    }
    
    @Test
    public void testUpdateHouseWithNullArgument() {

        House house = newHouse("House of Torment", new Address("Mexico", "Dark Shire", "Wall", "7"), 
                new Date(), "history");
        manager.createHouse(house);
        house.getId();
        
        try {
            manager.updateHouse(null);
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
    }

    @Test
    public void testDeleteHouse() {        
        House house = newHouse("House of Torment", new Address("Mexico", "Dark Shire", "Wall", "7"), 
                new Date(), "history");
        manager.createHouse(house);
        assertNotNull(manager.getHouse(house.getId()));
        manager.deleteHouse(house.getId());
        assertNull(manager.getHouse(house.getId()));                
    }

    @Test
    public void testDeleteGiftWithWrongAttributes() {
        House house = newHouse("House of Torment", new Address("Mexico", "Dark Shire", "Wall", "7"), 
                new Date(), "history");
        
        try {
            manager.deleteHouse(null);
            fail(); 
        } catch (IllegalArgumentException ex) {
            //OK
        }

        try {
            house.setId(null);
            manager.deleteHouse(house.getId());
            fail();
        } catch (IllegalArgumentException ex) {
            //OK
        }
    }
    
    @Test
    public void testGetAllHumansOfHouseAfterCreate() {
        House house = new House();
        Human human1 = new Human();
        human1.setName("Luke");
        house.addHuman(human1);
        Human human2 = new Human();
        human2.setName("Potter");        
        house.addHuman(human2);
        
        manager.createHouse(house);
        
        List<Human> expected = Arrays.asList(human1, human2);
        List<Human> actual = manager.getAllHumansOfHouse(house.getId());        

        assertEquals(expected, actual);
        assertDeepEqualsHuman(expected, actual);
    }
    
    @Test
    public void testGetAllHumansOfHouseAfterUpdate() {
        House house = new House();
        Human human = new Human();
        human.setName("Carl Drogo");        
        house.addHuman(human);        
        manager.createHouse(house);
        
        human.setName("Carl Drogo The Great");
        manager.updateHouse(house);        
        
        List<Human> humans = manager.getAllHumansOfHouse(house.getId());
        assertEquals("Carl Drogo The Great", humans.get(0).getName());
    }
    
    @Test
    public void testGetAllHumansOfHouseAfterDelete() {
        House house = new House();
        Human human = new Human();
        human.setName("Popeye");
        house.addHuman(human);
        
        manager.createHouse(house);     
        manager.deleteHouse(house.getId());     
        
        List<Human> humans = manager.getAllHumansOfHouse(house.getId()); 
        assertEquals(0, humans.size());
    }
    
    @Test
    public void testGetAllGhostsOfHouseAfterCreate() {
        House house = new House();
        Ghost ghost1 = new Ghost();
        ghost1.setName("Lucifer");
        house.addGhost(ghost1);
        Ghost ghost2 = new Ghost();
        ghost2.setName("Irenicus");
        house.addGhost(ghost2);
        
        manager.createHouse(house);
        
        List<Ghost> expected = Arrays.asList(ghost1, ghost2);
        List<Ghost> actual = manager.getAllGhostsOfHouse(house.getId());        

        assertEquals(expected, actual);
        assertDeepEqualsGhost(expected, actual);
    }
  
    @Test
    public void testGetAllGhostsOfHouseAfterUpdate() {
        House house = new House();
        Ghost ghost = new Ghost();
        ghost.setName("Frank");
        house.addGhost(ghost);        
        manager.createHouse(house);
        
        ghost.setName("Frank Enstein");
        manager.updateHouse(house);        
        
        List<Ghost> ghosts = manager.getAllGhostsOfHouse(house.getId());
        assertEquals("Frank Enstein", ghosts.get(0).getName());
    }
    
    @Test
    public void testGetAllGhostsOfHouseAfterDelete() {
        House house = new House();
        Ghost ghost = new Ghost();
        ghost.setName("Bloody Mary");
        house.addGhost(ghost);
        
        manager.createHouse(house);     
        manager.deleteHouse(house.getId());     
        
        List<Ghost> ghosts = manager.getAllGhostsOfHouse(house.getId()); 
        assertEquals(0, ghosts.size());
    }
    
/********************************PRIVATE METHODS************************************************/    
    
    private static House newHouse(String name, Address address, Date startOfHaunting, String history) {
        House house = new House();
        house.setName(name);
        house.setAddress(address);
        house.setStartOfHaunting(startOfHaunting);
        house.setHistory(history);
        return house;
    }
    
    public static void assertDeepEqualsHouse(List<House> expected, List<House> actual) {
        assertEquals(expected.size(), actual.size());
        List<House> expectedSortedList = new ArrayList<>(expected);
        List<House> actualSortedList = new ArrayList<>(actual);
        Collections.sort(expectedSortedList,idHouseComparator);
        Collections.sort(actualSortedList,idHouseComparator);
        for (int i = 0; i < expectedSortedList.size(); i++) {
            assertDeepEquals(expectedSortedList.get(i), actualSortedList.get(i));
        }
    }
    
    public static void assertDeepEqualsHuman(List<Human> expected, List<Human> actual) {
        assertEquals(expected.size(), actual.size());
        List<Human> expectedSortedList = new ArrayList<>(expected);
        List<Human> actualSortedList = new ArrayList<>(actual);
        Collections.sort(expectedSortedList,idHumanComparator);
        Collections.sort(actualSortedList,idHumanComparator);
        for (int i = 0; i < expectedSortedList.size(); i++) {
            assertEquals(expectedSortedList.get(i), actualSortedList.get(i));
        }
    }
    
    public static void assertDeepEqualsGhost(List<Ghost> expected, List<Ghost> actual) {
        assertEquals(expected.size(), actual.size());
        List<Ghost> expectedSortedList = new ArrayList<>(expected);
        List<Ghost> actualSortedList = new ArrayList<>(actual);
        Collections.sort(expectedSortedList,idGhostComparator);
        Collections.sort(actualSortedList,idGhostComparator);
        for (int i = 0; i < expectedSortedList.size(); i++) {
            assertEquals(expectedSortedList.get(i), actualSortedList.get(i));
        }
    }

    public static void assertDeepEquals(House expected, House actual) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getAddress(), actual.getAddress());
        assertEquals(expected.getHistory(), actual.getHistory());
        Date expectedDate = expected.getStartOfHaunting();
        Date actualDate = actual.getStartOfHaunting();
        if(expectedDate != null && actualDate != null) { 
            assertEquals(expectedDate.getYear(), actualDate.getYear());
            assertEquals(expectedDate.getMonth(), actualDate.getMonth());
            assertEquals(expectedDate.getDay(), actualDate.getDay());
        }
    }    
    
    private static Comparator<House> idHouseComparator = new Comparator<House>() {

        @Override
        public int compare(House o1, House o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    
    private static Comparator<Human> idHumanComparator = new Comparator<Human>() {

        @Override
        public int compare(Human o1, Human o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    
    private static final Comparator<Ghost> idGhostComparator = new Comparator<Ghost>() {

        @Override
        public int compare(Ghost o1, Ghost o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };    
}
