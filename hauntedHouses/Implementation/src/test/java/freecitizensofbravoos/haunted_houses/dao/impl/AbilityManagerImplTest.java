package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.haunted_houses.dao.AbilityManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author kika
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/testContext.xml")
public class AbilityManagerImplTest{
           
    @Autowired
    private AbilityManager ami;
    
    @PersistenceContext
    private EntityManager em ;  
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        for (Ability a: ami.getAbilities()) {
            ami.deleteAbility(a.getId());
        }
    }

    /**
     * Test of createAbility method, of class AbilityManagerImpl.
     */
    @Test
    public void testCreateAbility() {
        Ability a = new Ability ();
        String name = "Shock";
        a.setName(name);
        a.setDescription("Shock every electrical fetter in the same room");
        a.setSeverity(Severity.Fatal);
        ami.createAbility(a);
        Ability result = em.find(Ability.class,a.getId());
        assertEquals("Failed to create ability",result, a);
        assertEquals(result.getDescription(), a.getDescription());
        assertEquals(result.getId(), a.getId());
        assertEquals(result.getName(), a.getName());
        assertEquals(result.getSeverity(), a.getSeverity());
    }

    /**
     * Test of createAbility method, of class AbilityManagerImpl, with wrong parameters .
     */
    @Test
    public void testCreateAbilityWithWrongParameters() {        
        try {
            ami.createAbility(null);
            fail("ability = null created");
        } catch (IllegalArgumentException ex) {
            System.out.println("OK, ability = null did not created");
        }
        
        Ability a = new Ability ();
        a.setName(null);
        try {
            ami.createAbility(a);
            fail("ability created with no name");
        } catch (PersistenceException ex) {
            System.out.println("OK, ability did not create cause name was null !!!");
        }        
    }
    
    /**
     * Test of getAbility method, of class AbilityManagerImpl.
     */
    @Test
    public void testGetAbility() {
        System.out.println("getAbility");
        Ability a = new Ability ();
        a.setName("Expose Fears");
        a.setDescription("Expose Fears to every Human in the same room");
        a.setSeverity(Severity.Annoying);
        ami.createAbility(a);
        Ability result = ami.getAbility(a.getId());
        assertEquals("Failed to get ability",a, result);
        assertEquals(result.getDescription(), a.getDescription());
        assertEquals(result.getId(), a.getId());
        assertEquals(result.getName(), a.getName());
        assertEquals(result.getSeverity(), a.getSeverity());
    }

    /**
     * Test of updateAbility method, of class AbilityManagerImpl.
     */
   @Test
    public void testUpdateAbility() {
        System.out.println("updateAbility");
        Ability a = new Ability ();
        a.setName("Shattered Nerves");
        a.setDescription("Shatter Nerves to Human which saw him");
        a.setSeverity(Severity.Apocalyptic);
        ami.createAbility(a);
        
        a.setName("Delusion");
        a.setDescription("Despite him Human see his most horrible dream");
        a.setSeverity(Severity.Distressing);
        ami.updateAbility(a);
        
        assertEquals("Failed to update ability",a, ami.getAbility(a.getId()));
    }

    /**
     * Test of updateAbility method of class AbilityManagerImpl, with wrong parameters.
     */
    @Test
    public void testUpdateAbilityWithWrongParameters() {
        Ability ability = new Ability();
        ability.setName("Shattered Nerves");
        ami.createAbility(ability);
        Long abilityId = ability.getId();
        
        try {
            ami.updateAbility(null);
            fail("ability = null updated");
        } catch (IllegalArgumentException ex) {
            System.out.println("OK, ability = null did not update");
        }
    }    
    
    /**
     * Test of deleteAbility method, of class AbilityManagerImpl.
     */
   @Test
    public void testDeleteAbility() {
        System.out.println("deleteAbility");
        Ability a = new Ability ();
        a.setName("Uncover Fear");
        a.setDescription("Uncover Fear to every Human in the same room");
        a.setSeverity(Severity.Negligible);
        ami.createAbility(a);
        ami.deleteAbility(a.getId());
        assertEquals("Failed to delete ability",null, ami.getAbility(a.getId()));
       
    }
   
     /**
     * Test of deleteAbility method, of class AbilityManagerImpl, with wrong parameter.
     */
    @Test
    public void testDeleteAbilityWithWrongParameters() {
        Ability ability = new Ability();
        ability.setName("Shattered Nerves");
        ami.createAbility(ability);
        
        try {
            ami.deleteAbility(null);
            fail("ability = null deleted");
        } catch (IllegalArgumentException ex) {
            System.out.println("OK, ability = null did not delete");
        }
    }
    
    /**
     * Test of getAbilities method, of class AbilityManagerImpl.
     */
    @Test
    public void testGetAllAbilities() {
        Ability ability1 = new Ability();
        Ability ability2 = new Ability();
        
        ability1.setName("Uncover Fear");
        ability2.setName("Delusion");
        assertTrue(ami.getAbilities().isEmpty());
        
        ami.createAbility(ability1);
        ami.createAbility(ability2);
        
        List<Ability> expected = new ArrayList<>();
        expected.add(ability1);
        expected.add(ability2);
        
        List<Ability> result = ami.getAbilities();
        assertEquals(expected, result);
    }
}
