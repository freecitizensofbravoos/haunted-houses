package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.dao.HumanManager;
import freecitizensofbravoos.haunted_houses.entity.Human;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author xkostoln
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/testContext.xml")
public class HumanManagerImplTest {
    
    @Autowired
    private HumanManager hmi;
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        for (Human h : hmi.getAllHumans()) {
            hmi.deleteHuman(h.getId());
        }
    }

    /**
     * Test of createHuman method, of class HumanManagerImpl.
     * @throws java.text.ParseException
     */
    @Test
    public void testCreateHuman() throws java.text.ParseException {
        Human human = new Human();
        human.setName("Homer");
        human.setMedium(true);
        human.setAfraidOf("Marge, empty fridge, darkness");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
            human.setDateOfBirth(sdf.parse("1960.07.08"));
        hmi.createHuman(human);
        
        Long humanId = human.getId();
        assertNotNull(humanId);
        
        Human result = hmi.getHuman(humanId);
        assertEquals(result, human);
        assertEquals(result.getAfraidOf(),human.getAfraidOf());
        assertEquals(result.getDateOfBirth(), human.getDateOfBirth());
        assertEquals(result.getName(), human.getName());
        assertEquals(result.isMedium(), human.isMedium());        
    }
    
    /**
     * Test of createHuman method, of class HumanManagerImpl, with wrong parameters .
     */
    @Test
    public void testCreateHumanWithWrongParameters() {
        try {
            hmi.createHuman(null);
            fail();
        } catch (IllegalArgumentException ex) {
            System.out.println("OK");
        }
    }

    /**
     * Test of getHuman method, of class HumanManagerImpl.
     * @throws java.text.ParseException     */
    @Test
    public void testGetHuman() throws java.text.ParseException {
        assertNull(hmi.getHuman(new Long(100)));
        
        Human human = new Human();
        human.setName("Fico");
        human.setMedium(true);
        human.setAfraidOf("not beeing reelected");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        human.setDateOfBirth(sdf.parse("1965.07.01"));
        hmi.createHuman(human);
        
        Long humanId = human.getId();
        
        Human result = hmi.getHuman(humanId);
        assertEquals(result, human);
        assertEquals(result.getAfraidOf(),human.getAfraidOf());
        assertEquals(result.getDateOfBirth(), human.getDateOfBirth());
        assertEquals(result.getName(), human.getName());
        assertEquals(result.isMedium(), human.isMedium());
    }

    /**
     * Test of updateHuman method, of class HumanManagerImpl.
     * @throws java.text.ParseException
     */
    @Test
    public void testUpdateHuman() throws java.text.ParseException {
        Human human = new Human();
        human.setName("Gladiator");
        human.setMedium(true);
        human.setAfraidOf("safety of his family");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        human.setDateOfBirth(sdf.parse("1100.02.23"));
        
        hmi.createHuman(human);
        Long humanId = human.getId();
        //Set Name
        Human humanChange1 = hmi.getHuman(humanId);
        humanChange1.setName("Marcus Aurelius");
        hmi.updateHuman(humanChange1);
        Human result1 = hmi.getHuman(humanId);
        assertEquals(result1, humanChange1);
        assertEquals(result1.getName(), "Marcus Aurelius");
        
        //Set AfraidOf
        Human humanChange2 = hmi.getHuman(humanId);
        humanChange2.setAfraidOf("choose right succesor");
        hmi.updateHuman(humanChange2);
        Human result2 = hmi.getHuman(humanId);
        assertEquals(result2, humanChange2);
        assertEquals(result2.getAfraidOf(), "choose right succesor");
        
        //Set Date of Birth        
        Human humanChange3 = hmi.getHuman(humanId);
        humanChange3.setDateOfBirth(sdf.parse("1070.03.19"));
        hmi.updateHuman(humanChange3);
        Human result3 = hmi.getHuman(humanId);
        assertEquals(result3, humanChange3);
        assertEquals(result3.getDateOfBirth(), sdf.parse("1070.03.19"));
        
        //Set Medium
        Human humanChange4 = hmi.getHuman(humanId);
        humanChange4.setMedium(false);
        hmi.updateHuman(humanChange4);
        Human result4 = hmi.getHuman(humanId);
        assertEquals(result4, humanChange4);
        assertEquals(result4.isMedium(), false);
    }
    
    /**
     * Test of updateHuman method of class HumanManagerImpl, with wrong parameters.
     */
    @Test
    public void testUpdateHumanWithWrongParameters() {
        Human human = new Human();
        hmi.createHuman(human);
        Long humanId = human.getId();
        
        try {
            hmi.updateHuman(null);
            fail();
        } catch (IllegalArgumentException ex) {
            System.out.println("OK");
        }
    }
    
    /**
     * Test of deleteHuman method, of class HumanManagerImpl.
     * @throws java.text.ParseException
     */
    @Test
    public void testDeleteHuman() throws java.text.ParseException {        
        Human human = new Human();
        human.setName("Arya");
        human.setMedium(true);
        human.setAfraidOf("have no opportunity to avenge her father");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        human.setDateOfBirth(sdf.parse("1520.11.14"));
        
        hmi.createHuman(human);
        assertNotNull(hmi.getHuman(human.getId()));
        
        hmi.deleteHuman(human.getId());
        assertNull(hmi.getHuman(human.getId()));
    }
    
    /**
     * Test of deleteHuman method, of class HumanManagerImpl, with wrong parameter.
     */
    @Test
    public void testDeleteHumanWithWrongParameters() {
        Human human = new Human();
        hmi.createHuman(human);
        
        try {
            hmi.deleteHuman(null);
            fail();
        } catch (IllegalArgumentException ex) {
            System.out.println("OK");
        }
    }
    
    /**
     * Test of getAllHumans method, of class HumanManagerImpl.
     */
    @Test
    public void testGetAllHumans() {
        Human child = new Human();
        Human mother = new Human();
        
        child.setName("Mike");
        mother.setName("Clair");
        assertTrue(hmi.getAllHumans().isEmpty());
        
        hmi.createHuman(child);
        hmi.createHuman(mother);
        
        List<Human> expected = new ArrayList<>();
        expected.add(child);
        expected.add(mother);
        
        List<Human> result = hmi.getAllHumans();
        assertEquals(expected, result);
    }
}