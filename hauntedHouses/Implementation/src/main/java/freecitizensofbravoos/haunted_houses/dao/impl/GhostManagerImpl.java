package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.dao.GhostManager;
import java.util.List;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author xkaiser1
 */
@Repository
public class GhostManagerImpl implements GhostManager {

    @PersistenceContext
    protected EntityManager em;
    
    @Override
    public void createGhost(Ghost ghost){
        em.persist(ghost);    
        em.close();
    }
    
    @Override
    public Ghost getGhost(Long id){
        Ghost result = em.find(Ghost.class, id);
        em.close();
        return result;
    }
    
    @Override
    public void updateGhost(Ghost ghost){
        em.merge(ghost);    
        em.close();
    }
    
    @Override
    public void deleteGhost(Long id){
        Ghost ghost = em.find(Ghost.class, id);
        if (ghost != null) {
            em.remove(ghost); 
        }
        em.close();
    }
    
    @Override
    public List<Ghost> getAllGhosts(){
        List<Ghost> ghosts = em.createQuery("SELECT g from Ghost g",Ghost.class).getResultList();
        em.close();
        return ghosts;
    }
    
    @Override
    public Set<Ability> getAllAbilitiesOfGhost(Long id){
        Set<Ability> abilities = em.find(Ghost.class, id).getAbilities();
        em.close();
        return abilities;
    }
}
