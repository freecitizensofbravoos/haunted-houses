package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.dao.HouseManager;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author xkostoln
 */
@Repository
public class HouseManagerImpl implements HouseManager {

    @PersistenceContext
    protected EntityManager em;
        
    @Override
    public void createHouse(House house) {
        em.persist(house);
        em.close();
    }

    @Override
    public House getHouse(Long id) {
        House house = em.find(House.class, id);
        em.close();
        return house;
    }

    @Override
    public void updateHouse(House house) {
        em.merge(house);
        em.close();
    }

    @Override
    public void deleteHouse(Long id) {
        House house = em.find(House.class, id);
        if (house != null) {
            em.remove(house);
        }
        em.close();
    }
    
    @Override
    public List<Human> getAllHumansOfHouse(Long id) {
        List<Human> humans = em.createQuery("SELECT a from House h JOIN h.humans a WHERE "
                + "h.id=:houseid",Human.class).setParameter("houseid",id).getResultList();
        em.close();
        return humans;
    }

    @Override
    public List<Ghost> getAllGhostsOfHouse(Long id) {
        List<Ghost> ghosts = em.createQuery("SELECT g from House h JOIN h.ghosts g WHERE h.id=:houseid",Ghost.class).setParameter("houseid",id).getResultList();
        em.close();
        return ghosts;
    }
    
    @Override
    public List<House> getAllHouses() {
        List<House> houses = em.createQuery("SELECT h FROM House h",House.class).getResultList();
        em.close();
        return houses;
    }
}