package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.dao.UserDataManager;
import freecitizensofbravoos.haunted_houses.entity.UserData;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Matus
 */
public class UserDataManagerImpl implements UserDataManager{

    @PersistenceContext
    protected EntityManager em;  
    
    @Override
    public void createUserData(UserData userData) {
        em.persist(userData);   
        em.close();
    }

    @Override
    public UserData getUserData(String login) {
        UserData result = em.find(UserData.class, login);
        em.close();
        return result;
    }

    @Override
    public void updateUserData(UserData userData) {        
        em.merge(userData);
        em.close();
    }    

    @Override
    public void deleteUserData(String login) {
        UserData userData = em.find(UserData.class, login);
        if (userData != null) {
            em.remove(userData); 
        }
        em.close();
    }

    @Override
    public List<UserData> getAllUserData() {
        List<UserData> userData = em.createQuery("SELECT ud FROM UserData ud", UserData.class).getResultList();
        em.close();
        return userData;
    }    
}
