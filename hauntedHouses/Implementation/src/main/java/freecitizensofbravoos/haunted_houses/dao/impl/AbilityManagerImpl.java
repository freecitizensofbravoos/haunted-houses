package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.dao.AbilityManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author xmoravc6
 */
@Repository
public class AbilityManagerImpl implements AbilityManager {
    
    @PersistenceContext
    protected EntityManager em;  
    
    @Override
    public void createAbility(Ability ability) {  
        em.persist(ability);//GRO    
        em.close();
    }

    @Override
    public Ability getAbility(Long id) {
        Ability result = null;
        result = em.find(Ability.class, id);//GRO
        em.close();
        return result;
    }

    @Override
    public void updateAbility(Ability ability) {
        em.merge(ability);//GRO  
        em.close();
    }

    @Override
    public void deleteAbility(Long id) {
        Ability ability = em.find(Ability.class, id);
        if (ability != null) {
            em.remove(ability); 
        }
        em.close();
    } 
    
    @Override
    public List<Ability> getAbilities() {
        List<Ability> abilities = null;
        abilities = em.createQuery("SELECT a FROM Ability a", Ability.class).getResultList();
        em.close();
        return abilities;
    }
    
    @Override
    public List<Ghost> getAllGhosts(Long id){
        List<Ghost> ghosts = em.createQuery("SELECT a from Ability h JOIN h.ghosts a WHERE "
                + "h.id=:ghostid", Ghost.class).setParameter("ghostid",id).getResultList();
        em.close();
        return ghosts;
    }
}
