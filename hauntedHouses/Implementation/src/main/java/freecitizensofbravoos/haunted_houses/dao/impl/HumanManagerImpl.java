package freecitizensofbravoos.haunted_houses.dao.impl;

import freecitizensofbravoos.haunted_houses.dao.HumanManager;
import freecitizensofbravoos.haunted_houses.entity.Human;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author xurge
 */
@Repository
public class HumanManagerImpl implements HumanManager {
 
    @PersistenceContext
    private EntityManager em;

    @Override
    public void createHuman(Human human) {
        em.persist(human);
        em.close();
    }

    @Override
    public Human getHuman(Long id) {
        Human human;
        human = em.find(Human.class, id);
        em.close();
        return human;
    }

    @Override
    public void updateHuman(Human human) {
        em.merge(human);
        em.close();
    }

    @Override
    public void deleteHuman(Long id) {
        Human human = em.find(Human.class, id);
        if (human != null) {
            em.remove(human);
        }
        em.close();
    }

    @Override
    public List<Human> getAllHumans() { 
        List<Human> humans;
        humans = em.createQuery("SELECT h FROM Human h", Human.class).getResultList();
        em.close();
        return humans;
    }    
}
