package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.dao.GhostManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.service.GhostService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author xmoravc6
 */
public class GhostServiceImpl implements GhostService{
    
    private GhostManager manager;
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

    public GhostServiceImpl() { } 

    public void setManager(GhostManager manager)
    {
        this.manager = manager;
    }
    
    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    public List<GhostDTO> findAllGhosts()
    {
        List<GhostDTO> resultList = new ArrayList<>();
        for(Ghost ghost : manager.getAllGhosts())
        {
            resultList.add(mapToGhostDTO(ghost));
        }        
        return resultList;
    }    
    
    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    public List<AbilityDTO> findAbilitiesOfGhost(Long id)
    {
        Ghost ghost = manager.getGhost(id);
        if(ghost == null)
        {
            return null;
        }
        ArrayList<AbilityDTO> resultList = new ArrayList<>();
        for(Ability a : ghost.getAbilities())
        {
            resultList.add(mapper.map(a, AbilityDTO.class));
        }        
        return resultList;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createGhost(GhostDTO ghostDTO)
    {
        if(ghostDTO == null)
        {
            throw new IllegalArgumentException("Given ghost cannot be null!");
        }
        manager.createGhost(mapToGhost(ghostDTO));
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateGhost(GhostDTO ghostDTO)
    {
        if(ghostDTO == null)
        {
            throw new IllegalArgumentException("Given ghost cannot be null!");
        }
        manager.updateGhost(mapToGhost(ghostDTO));
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteGhost(Long id)
    {
        manager.deleteGhost(id);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    public GhostDTO findGhost(Long id)
    {
        Ghost ghost = manager.getGhost(id);
        if(ghost == null)
        {
            return null;
        }
        return mapToGhostDTO(ghost);
    }

    @Override
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    public List<GhostDTO> findGhostsByName(String name)
    {
        List<GhostDTO> resultList = new ArrayList<>();
        for(Ghost ghost : manager.getAllGhosts())
        {
            if(Objects.equals(ghost.getName(), name))
            {
                resultList.add(mapToGhostDTO(ghost));
            }
        }
        return resultList;
    }
    
    private Ghost mapToGhost(GhostDTO ghostDTO)
    {
        Ghost ghost = mapper.map(ghostDTO, Ghost.class);
        HouseDTO houseDTO = ghostDTO.getHouse();
        if (houseDTO != null)
        {
            ghost.setHouse(mapper.map(houseDTO, House.class));
        }
        
        Set<AbilityDTO> abilitiesDTO = ghostDTO.getAbilities();
        if (abilitiesDTO != null)
        {
            for(AbilityDTO aDTO : abilitiesDTO)
            {
                ghost.addAbility(mapper.map(aDTO, Ability.class));
            }
        }
        return ghost;
    }
    
    private GhostDTO mapToGhostDTO(Ghost ghost)
    {
        GhostDTO ghostDTO = mapper.map(ghost, GhostDTO.class);
        House house = ghost.getHouse();
        if (house != null)
        {
            ghostDTO.setHouse(mapper.map(house, HouseDTO.class));
        }
        
        Set<Ability> abilities = ghost.getAbilities();
        if (abilities != null)
        {
            for(Ability a : abilities)
            {
                ghostDTO.addAbility(mapper.map(a, AbilityDTO.class));
            }
        }
        return ghostDTO;
    }
}
