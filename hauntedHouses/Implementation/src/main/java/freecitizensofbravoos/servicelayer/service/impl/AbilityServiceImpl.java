package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.Severity;
import freecitizensofbravoos.haunted_houses.dao.AbilityManager;
import freecitizensofbravoos.haunted_houses.dao.GhostManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.service.AbilityService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author xkostoln
 */
public class AbilityServiceImpl implements AbilityService {
    private AbilityManager manager;
    private GhostManager ghostManager;
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

    public AbilityServiceImpl() { }

    public void setManager(AbilityManager manager) {
        this.manager = manager;
    }

    public void setGhostManager(GhostManager ghostManager) {
        this.ghostManager = ghostManager;
    } 
    
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<AbilityDTO> findAllAbilities() {
        List<AbilityDTO> result = new ArrayList<> ();
        for (Ability ability : manager.getAbilities())
        {
            result.add(mapper.map(ability, AbilityDTO.class));
        }
        return result;
    }
    
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<AbilityDTO> findAbilitiesBySeverity(Severity severity) {
        List<AbilityDTO> result = new ArrayList<> ();
        for (Ability ability : manager.getAbilities())
        {
            if (Objects.equals(severity, ability.getSeverity()))
            {
                result.add(mapper.map(ability, AbilityDTO.class));
            }
        }
        return result;
    }
    
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void createAbility(AbilityDTO abilityDTO) {
        if(abilityDTO == null)
            throw new IllegalArgumentException("null parameter");
        Ability ability = mapper.map(abilityDTO, Ability.class);
        manager.createAbility(ability);
    }
    
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void updateAbility(AbilityDTO abilityDTO) {
        if(abilityDTO == null)
            throw new IllegalArgumentException("null parameter");
        Ability ability = mapper.map(abilityDTO, Ability.class);
        manager.updateAbility(ability);
    }
    
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void deleteAbility(Long id) {
        Ability ability = manager.getAbility(id);
        if(manager.getAbility(id) != null)
        {        
            for(Ghost g : manager.getAllGhosts(id))
            {
                g.removeAbility(ability);
                ghostManager.updateGhost(g);
            }     
        }
        manager.deleteAbility(id);
    }
    
    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public AbilityDTO findAbility(Long id) {
        Ability ability = manager.getAbility(id);
        if (ability == null)
            return null;
        AbilityDTO abilityDTO = mapper.map(ability, AbilityDTO.class);
        return abilityDTO;
    }
}
