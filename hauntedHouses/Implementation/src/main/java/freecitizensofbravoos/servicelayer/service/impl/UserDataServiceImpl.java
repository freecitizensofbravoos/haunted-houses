package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.dao.UserDataManager;
import freecitizensofbravoos.haunted_houses.entity.UserData;
import freecitizensofbravoos.servicelayer.dto.UserDataDTO;
import freecitizensofbravoos.servicelayer.service.UserDataService;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * @author Matus
 */
public class UserDataServiceImpl implements UserDataService, UserDetailsService {
    private UserDataManager manager;    
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance(); 
    private final static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
    
    public UserDataServiceImpl() { }

    public void setManager(UserDataManager manager) {
        this.manager = manager;
    }
    
    @Transactional
    @Override
    public void createUserData(UserDataDTO userDataDTO) {
        if(userDataDTO == null)
            throw new IllegalArgumentException("null parameter");
        UserData userData = mapper.map(userDataDTO, UserData.class);
        userData.setPassword(encoder.encode(userDataDTO.getPassword()));        
        
        manager.createUserData(userData);
    }
    
    @Transactional(readOnly=true)
    @Override
    public UserDataDTO findUserData(String login) {
        UserData userData = manager.getUserData(login);
        if (userData == null)
            return null;
        UserDataDTO userDataDTO = mapper.map(userData, UserDataDTO.class);
        return userDataDTO;
    }

    @Transactional
    @Override
    public void updateUserDataExceptPassword(UserDataDTO userDataDTO) {
        if(userDataDTO == null)
            throw new IllegalArgumentException("null parameter");        
        UserData userData = mapper.map(userDataDTO, UserData.class);
        String password = manager.getUserData(userDataDTO.getUsername()).getPassword();
        userData.setPassword(password);
        manager.updateUserData(userData);
    }
    
    @Transactional
    @Override
    public void updateUserPassword(String login, String newPassword) {
        if(login == null || newPassword == null)
            throw new IllegalArgumentException("null parameter");        
        UserData userData = manager.getUserData(login);
        userData.setPassword(encoder.encode(newPassword));
        manager.updateUserData(userData);
    }

    @Transactional
    @Override
    public void deleteUserData(String login) {
        manager.deleteUserData(login);
    }
    
    @Transactional(readOnly=true)
    @Override
    public List<UserDataDTO> getAllUserData() {
        List<UserDataDTO> result = new ArrayList<>();
        for (UserData userData : manager.getAllUserData())
        {
            result.add(mapper.map(userData, UserDataDTO.class));
        }
        return result;
    }    

    @Override
    public UserDetails loadUserByUsername(String username)  {
        UserDataDTO userData = findUserData(username);
        GrantedAuthority authority = new SimpleGrantedAuthority(userData.getAuthorities());
        List<GrantedAuthority> userAuthorities = new ArrayList<>();
        userAuthorities.add(authority);
        UserDetails user = new User(username, userData.getPassword(), userAuthorities);
        return user;
    }
}
