package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.dao.HouseManager;
import freecitizensofbravoos.haunted_houses.entity.Ability;
import freecitizensofbravoos.haunted_houses.entity.Ghost;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import freecitizensofbravoos.servicelayer.dto.AbilityDTO;
import freecitizensofbravoos.servicelayer.dto.GhostDTO;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import freecitizensofbravoos.servicelayer.service.HouseService;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author xurge
 */
public class HouseServiceImpl implements HouseService {
    
    private HouseManager manager;
    
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

    public void setManager(HouseManager manager) {
        this.manager = manager;
    }

    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<HouseDTO> findAllHouses() {
        List<HouseDTO> result = new ArrayList<>();
        for(House h : manager.getAllHouses()) {
            result.add(mapper.map(h, HouseDTO.class));
        }
        return result;
    }    

    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<HouseDTO> findHousesByCountry(String country) {
        List<HouseDTO> result = new ArrayList<>();
        for(House h : manager.getAllHouses()) {
            if(h.getAddress().getAddrState().equals(country)) {
                result.add(mapper.map(h, HouseDTO.class));
            }
        }
        return result;
    }

    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<GhostDTO> findGhostsOfHouse(HouseDTO house) {
        List<GhostDTO> result = new ArrayList<>();
        List<Ghost> ghosts = manager.getAllGhostsOfHouse(house.getId());
        for(Ghost g : ghosts) {
            GhostDTO ghost = mapper.map(g, GhostDTO.class);
            for (Ability a : g.getAbilities()) {
                ghost.addAbility(mapper.map(a, AbilityDTO.class));
            }
            result.add(ghost);
        }
        return result;
    }

    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public List<HumanDTO> findHumansOfHouse(HouseDTO house) {
        List<HumanDTO> result = new ArrayList<>();
        List<Human> humans = manager.getAllHumansOfHouse(house.getId());
        for(Human h : humans) {
            result.add(mapper.map(h, HumanDTO.class));
        }
        return result;
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void createHouse(HouseDTO houseDTO) {
        if (houseDTO == null) {
            throw new IllegalArgumentException("HouseDTO can not be null");
        }
        House house = mapToHouse(houseDTO);
        manager.createHouse(house);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void updateHouse(HouseDTO houseDTO) {
        if (houseDTO == null) {
            throw new IllegalArgumentException("HouseDTO can not be null");
        }
        House house = mapToHouse(houseDTO);
        manager.updateHouse(house);
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @Override
    public void deleteHouse(Long id) {
        manager.deleteHouse(id);
    }

    @Transactional(readOnly=true)
    @PreAuthorize("isAuthenticated()")
    @Override
    public HouseDTO findHouse(Long id) {
        House house = manager.getHouse(id);
        if (house == null) {
            return null;
        }        
        HouseDTO houseDTO = mapper.map(house, HouseDTO.class);
        return houseDTO;
    }
    
    private House mapToHouse(HouseDTO houseDTO) {
        House house = mapper.map(houseDTO, House.class);
        for (HumanDTO h : houseDTO.getHumans()) {
            Human human = mapper.map(h, Human.class);
            house.addHuman(human);
        }
        for (GhostDTO g : houseDTO.getGhosts()) {
            Ghost ghost = mapper.map(g, Ghost.class);
            for (AbilityDTO a : g.getAbilities()) {
                ghost.addAbility(mapper.map(a, Ability.class));
            }
            house.addGhost(ghost);
        }
        return house;
    }    
}
