package freecitizensofbravoos.servicelayer.service.impl;

import freecitizensofbravoos.haunted_houses.dao.HumanManager;
import freecitizensofbravoos.haunted_houses.entity.House;
import freecitizensofbravoos.haunted_houses.entity.Human;
import freecitizensofbravoos.servicelayer.dto.HouseDTO;
import freecitizensofbravoos.servicelayer.dto.HumanDTO;
import freecitizensofbravoos.servicelayer.service.HumanService;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kika
 */
public class HumanServiceImpl implements HumanService{

    
    private HumanManager manager;
    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

    public HumanServiceImpl() { } 

    public void setManager(HumanManager manager) {
        this.manager = manager;
    }
    
    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("isAuthenticated()")
    public List<HumanDTO> findAllHumans() {
        List<HumanDTO> resultList = new ArrayList<>();
        for(Human human : manager.getAllHumans())
        {
            resultList.add(mapToHumanDTO(human));
        }        
        return resultList;
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("isAuthenticated()")
    public List<HumanDTO> findAllMediums() {
        List<HumanDTO> resultList = new ArrayList<>();
        for(Human human : manager.getAllHumans())
        {
            if (human.isMedium()){
                resultList.add(mapToHumanDTO(human));
            }
        }        
        return resultList;
    }
  
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createHuman(HumanDTO humanDTO) {
        if (humanDTO == null)
            throw new IllegalArgumentException("Null Argument");
        Human human = mapToHuman(humanDTO);
        manager.createHuman(human);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateHuman(HumanDTO humanDTO) {
        if (humanDTO == null)
            throw new IllegalArgumentException("Null Argument");
        Human human = mapToHuman(humanDTO);
        manager.updateHuman(human);
    }
    
    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void deleteHuman(Long id) {
        manager.deleteHuman(id);
    }

    @Override
    @Transactional(readOnly = true)
    @PreAuthorize("isAuthenticated()")
    public HumanDTO findHuman(Long id) {
        Human human = manager.getHuman(id);
        if(human == null)
        {
            return null;
        }
        HumanDTO humanDTO = mapToHumanDTO(human);
        return humanDTO;
    }
    
    private Human mapToHuman(HumanDTO humanDTO) {
        Human human = mapper.map(humanDTO, Human.class);
        HouseDTO houseDTO = humanDTO.getHouse();
        if (houseDTO != null) {
            House house = mapper.map(houseDTO, House.class);
            human.setHouse(house);
        }
        return human;
    }
    
    private HumanDTO mapToHumanDTO(Human human) {
        HumanDTO humanDTO = mapper.map(human, HumanDTO.class);
        House house = human.getHouse();
        if (house != null) {
            HouseDTO houseDTO = mapper.map(house, HouseDTO.class);
            humanDTO.setHouse(houseDTO);
        }
        return humanDTO;
    }    
}
